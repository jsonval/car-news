package com.ruiao.car.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Vincent.Pei
 * @date 2019/3/8上午11:15
 * @Description:
 */
@Slf4j
public class UnLoginException extends Exception{

    public UnLoginException(String message) {
        super(message);
    }

}

