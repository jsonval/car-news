package com.ruiao.car.utils;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Author  : RandySun (sunfeng152157@sina.com)
 * Date    : 2018-06-20  15:53
 * Comment :
 */
@Slf4j
public class PartitionKeyUtil {
    public static final String RULE_YYYYMM = "yyyyMM";
    public static final String RULE_YYYYMMDD = "yyyyMMdd";



    /**
     * @param rule 生成规则
     * @return 结果 partitionKey
     */
    public static final Integer genPartitionKey(String rule,Date date){
        Integer partitionKey = null;
        try {
            partitionKey = Integer.valueOf(PartitionKeyUtil.formatDateString(rule,date));
        }catch (Exception e){
            log.error("genPartitionKey error",e);
        }
        return partitionKey;
    }

    /**
     * @return 结果　partitionKey
     */
    public static final Integer genPartitionMonth(){
        return genPartitionKey(RULE_YYYYMM,new Date());
    }

    public static final Integer genPartitionDay(){
        return genPartitionKey(RULE_YYYYMMDD,new Date());
    }


    public static final Integer genPartitionKey(Date date){
        return genPartitionKey(RULE_YYYYMMDD,date);
    }


    public static String formatDateString(String rule,Date date) {
        if (date == null) {
            return "";
        }
        return new SimpleDateFormat(rule).format(date).toString();
    }

//    public static void main(String args[]){
//
//        Date startTime = DateUtils.parseDateTime("2020-03-01 10:00:00");
//        Date yesterday = DateUtils.addDay(-1);
//        Long maxMinus = DateUtils.diffDay(startTime,yesterday )*24*60 ;
//        System.out.println(maxMinus);
//
//    }


}
