package com.ruiao.car.utils;


import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Map;

/**
 * 数字签名工具类
 */
public class Digests {
    /**
     *
     * 检查签名
     *
     * @author JoeHe
     * @since 2014年5月7日
     * @since 1.0
     *
     * @param signature
     * @param tmpArr
     * @return boolean
     *
     */
    public static boolean checkSignature(String signature, String... tmpArr) {

        return signature.equalsIgnoreCase(genSignature(tmpArr));
    }

    /**
     *
     * 生成签名
     *
     * @author JoeHe
     * @since 2014年5月7日
     * @since 1.0
     *
     * @param tmpArr
     * @return String
     *
     */
    public static String genSignature(String... tmpArr) {

        Arrays.sort(tmpArr);
        String tmpStr = StringUtils.join(tmpArr, "");
        return DigestUtils.sha1Hex(tmpStr);
    }

    /**
     *
     * 生成签名
     *
     * @author JoeHe
     * @since 2014年5月7日
     * @since 1.0
     *
     * @param parMap
     * @return String
     *
     */
    public static String genSignature(Map<String, String> parMap) {

        String[] str = new String[parMap.size()];
        return genSignature(parMap.values().toArray(str));
    }

    /**
     *
     * 返回md5码
     *
     * @author JoeHe
     * @since 2014年5月7日
     * @since 1.0
     *
     * @param data
     * @return String
     *
     */
    public static String md5Hex(String data) {

        return DigestUtils.md5Hex(data);
    }

    /**
     *
     * 返回md5码
     *
     * @author JoeHe
     * @since 2014年5月7日
     * @since 1.0
     *
     * @param data
     * @return String
     *
     */
    public static String md5Hex(byte[] data) {

        return DigestUtils.md5Hex(data);
    }

    /**
     *
     * 返回sha1签名
     *
     * @author JoeHe
     * @since 2014年5月9日
     * @since 1.0
     *
     * @param data
     * @return String
     *
     */
    public static String sha1(String data) {

        return DigestUtils.sha1Hex(data);
    }

    public static String sha256Hex(String data) {

        return DigestUtils.sha256Hex(data);
    }

    /**
     *
     * 返回sha1签名
     *
     * @author JoeHe
     * @since 2014年5月9日
     * @since 1.0
     *
     * @param data
     * @return String
     *
     */
    public static String sha1(byte[] data) {

        return DigestUtils.sha1Hex(data);
    }

}
