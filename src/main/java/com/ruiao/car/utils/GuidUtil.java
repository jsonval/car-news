package com.ruiao.car.utils;

import java.util.UUID;

/**
 * Created by jsonval on 2019/10/18.
 */
public class GuidUtil {
    public static String genGuid(){
        return UUID.randomUUID().toString().replace("-", "").toLowerCase();
    }

}
