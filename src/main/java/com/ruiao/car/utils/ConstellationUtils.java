package com.ruiao.car.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by jsonval on 2019/11/29.
 */
public class ConstellationUtils {

    private final static int[] dayArr = new int[]{20, 19, 21, 20, 21, 22, 23, 23, 23, 24, 23, 22};
    private final static String[] constellationArr = new String[]{"摩羯座", "水瓶座", "双鱼座", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "摩羯座"};

    public static String getConstellation(LocalDateTime date) {
        if (date == null) {
            return null;
        }
        int day = date.getDayOfMonth();
        int month = date.getMonthValue();
        return day < dayArr[month - 1] ? constellationArr[month - 1] : constellationArr[month];
    }


    public static String age(LocalDateTime startTime) {
        if (startTime == null) {
            return null;
        }
        java.time.Duration duration = java.time.Duration.between(startTime, LocalDateTime.now());
        return String.valueOf(duration.toDays() / 365);
    }


    public static String getConstellation(String time) {
        LocalDateTime date = conver(time);
        if (date == null) {
            return null;
        }
        int day = date.getDayOfMonth();
        int month = date.getMonthValue();
        return day < dayArr[month - 1] ? constellationArr[month - 1] : constellationArr[month];
    }


    public static String age(String time) {
        LocalDateTime startTime = conver(time);
        if (startTime == null) {
            return null;
        }
        java.time.Duration duration = java.time.Duration.between(startTime, LocalDateTime.now());
        return String.valueOf(duration.toDays() / 365);
    }


    public static LocalDateTime conver(String data) {
        try {
            Date date = DateUtils.parseDate(data, DateUtils.DATE_FORMAT_DEFAULT);
            Instant instant = date.toInstant();
            ZoneId zone = ZoneId.systemDefault();
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
            return localDateTime;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static void main(String[] args) {
        LocalDateTime time = conver("2018-11-25");
//
//        System.out.println("月" + time.getMonthValue());
//        System.out.println("日" + time.getDayOfMonth());
//        System.out.println(getConstellation(time));
//        System.out.println(age(time));
        System.out.println(getConstellation("2018-11-25"));
        System.out.println(age("2018-11-25"));


    }
}
