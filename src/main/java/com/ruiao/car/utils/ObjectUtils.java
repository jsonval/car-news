package com.ruiao.car.utils;

/**
 * Created by jsonval on 2019/10/29.
 */
public class ObjectUtils {

    public static boolean isNotNull(Object ob) {
        return !isNull(ob);
    }

    public static boolean isNull(Object... obs) {
        for (Object ob : obs) {
            if (ob == null) {
                return true;
            }
        }
        return false;
    }
}
