package com.ruiao.car.utils;

import java.math.BigDecimal;

/**
 * Created by jsonval on 2020/5/8.
 */
public class LevelUtil {

    public static String getLevel(Integer level) {
       /* if (level == null) {
            return "普通用户";
        }
        if (level == 0) {
            return "普通用户";
        }
        if (level == 1) {
            return "一星用户";
        }
        if (level == 2) {
            return "二星用户";
        }
        if (level == 3) {
            return "三星用户";
        }
        if (level == 4) {
            return "四星用户";
        }

        return "普通用户";*/
        if (level == null) {
            return "0";
        }
        return level.toString();

    }


    /**
     * 获取用户等级
     *
     * @param validTkerNum
     * @param groupAmount
     * @return
     */
    public static int getLevel(int validTkerNum, BigDecimal groupAmount) {
        //普通用户
        if (validTkerNum < 20 || groupAmount == null) {
            return 0;
        }
        if (groupAmount.compareTo(new BigDecimal(1000)) >= 0 && groupAmount.compareTo(new BigDecimal(10000)) < 0) {
            return 1;

        } else if (groupAmount.compareTo(new BigDecimal(10000)) >= 0 && groupAmount.compareTo(new BigDecimal(100000)) < 0) {
            return 2;

        } else if (groupAmount.compareTo(new BigDecimal(100000)) >= 0 && groupAmount.compareTo(new BigDecimal(1000000)) < 0) {

            return 3;
        } else if (groupAmount.compareTo(new BigDecimal(1000000)) >= 0) {

            return 4;
        }
        return 0;

    }
}
