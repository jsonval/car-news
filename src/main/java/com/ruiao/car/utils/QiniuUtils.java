package com.ruiao.car.utils;

import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.qiniu.util.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * @author yinpingliu
 * @name QiniuUtils
 * @time: 2020/6/16 下午5:32
 * @description: No Description
 */
public class QiniuUtils {
    public static final String ACCESS_KEY = "JfPwYykKAOh3WClAOqDPey7r5RdM5WmCs7JBJHQX";
    public static final String SECRET_KEY = "EcxIgYzWYv2G1Col0VCwkrwexhG8fdZ1SjiIbFAm";
    public static final String BUCKET_NAME = "ra-image";
    public static final String IMAGE_PRE = "http://raimage.shiziqiu.com/";

    public static final Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
    public static final Configuration c = new Configuration(Zone.zone0());
    public static final UploadManager uploadManager = new UploadManager(c);
    public static final BucketManager bucketManager = new BucketManager(auth,c);

    public static String getUpToken() {
        return auth.uploadToken(BUCKET_NAME, null, 3600, null);
    }

    public static String upload(MultipartFile file,String key){
        try {
            byte[] uploadBytes = file.getBytes();
            ByteArrayInputStream byteInputStream=new ByteArrayInputStream(uploadBytes);
            try {
                Response response = uploadManager.put(byteInputStream,key,getUpToken(),null, null);
                //解析上传成功的结果
                DefaultPutRet putRet = GsonUtils.get().fromJson(response.bodyString(), DefaultPutRet.class);
                return IMAGE_PRE+key;
            } catch (QiniuException ex) {
                Response r = ex.response;
                System.err.println(r.toString());
                try {
                    System.err.println(r.bodyString());
                } catch (QiniuException ex2) {
                    //ignore
                }
            }
        } catch (UnsupportedEncodingException ex) {
            //ignore
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String fetchImage(String otherOriginImageUr){
        if(StringUtils.isNullOrEmpty(otherOriginImageUr)){
            return Strings.EMPTY;
        }
        String imageUrl = ImageUtil.generateImageNamePre()+otherOriginImageUr.substring(otherOriginImageUr.lastIndexOf("."));
        try {
            bucketManager.fetch(otherOriginImageUr,BUCKET_NAME,imageUrl);
        } catch (QiniuException e) {
            e.printStackTrace();
        }
        return IMAGE_PRE+imageUrl;
    }

    public static void main(String[] args) throws QiniuException {
//        bucketManager.fetch("https://www2.autoimg.cn/chejiahaodfs/g30/M0A/D4/E1/autohomecar__ChsEf17pyviANAqPAADT8lE6qac819.png",BUCKET_NAME,"tt1111");

        String html="<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>领克作为一个年轻的汽车品牌，它的表现想必车友们都是有目共睹的。领克诞生于2016年，在这几年里它凭借原创度极高的个性设计给大家留下了深刻印象，同时也完成了从紧凑级SUV到紧凑型轿车、再到轿跑SUV的产品布局。今天我在三亚，有幸见到了刚刚亮相的 <span class=\"word-ser-span\" data-sercarid=\"5555\" data-word=\"领克06\">领克06</span>，一起来看看它有什么新鲜味道。</p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g30/M0A/D4/E1/autohomecar__ChsEf17pyviANAqPAADT8lE6qac819.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g30/M0A/D4/E1/autohomecar__ChsEf17pyviANAqPAADT8lE6qac819.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>快速了解领克06<br></p>\n" +
                " <p>1、外观设计家族基因明显2、内饰变化较大3、长宽高分别为4340/1820/1625mm，轴距2640mm4、燃油/插混/48V轻混，三种动力版本</p>\n" +
                " <p>家族风格依旧，细节更动感</p>\n" +
                " <p>领克06定位于入门的紧凑型SUV，外观方面依然有着非常浓厚的领克基因。06的前脸和”哥哥们“一样，采用了分体式的大灯设计，在保证了前脸层次感的同时也极大的提高了辨识度。事实上这一设计已经成了领克家族的招牌所在，乍一看有些特立独行的造型已经被更多消费者认可。</p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M08/2E/38/autohomecar__ChwFj17pyvmAf8YGAACqhIkf9Kg346.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M08/2E/38/autohomecar__ChwFj17pyvmAf8YGAACqhIkf9Kg346.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p><br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g30/M03/D4/E1/autohomecar__ChsEf17pyvmAMBntAACqSNXk62o970.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g30/M03/D4/E1/autohomecar__ChsEf17pyvmAMBntAACqSNXk62o970.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>值得一提的是此次的实拍车型采用了橙蓝撞色的设计，新车在进气格栅、轮毂和窗线等位置采用了橙色饰条的点缀，视觉上很有活力。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M06/36/F7/autohomecar__ChsEel7pyvqAfMQ9AACcrB6fVWQ423.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M06/36/F7/autohomecar__ChsEel7pyvqAfMQ9AACcrB6fVWQ423.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>大灯部分，从上到下分别为日间行车灯和主大灯组。被称为”北极之光“的日行灯造型新颖辨识度高。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g30/M06/0F/00/autohomecar__ChwFlF7pyvqAUDUbAAChK_xdFF8432.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g30/M06/0F/00/autohomecar__ChwFlF7pyvqAUDUbAAChK_xdFF8432.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x384\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p><br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M0B/36/F7/autohomecar__ChsEel7pyvqABxB2AACf5wOyjOo911.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M0B/36/F7/autohomecar__ChsEel7pyvqABxB2AACf5wOyjOo911.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>大灯和进气格栅相连的设计也使得整个前脸造型不会过于复杂，格栅内部采用了和05相仿的烤漆装饰，视觉感受比较精致。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M00/2E/39/autohomecar__ChwFj17pyvuAcUlAAAC1p_RBqTE746.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M00/2E/39/autohomecar__ChwFj17pyvuAcUlAAAC1p_RBqTE746.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p><br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g2/M00/D5/5B/autohomecar__ChsEkF7pyvuAf0nuAADGhWtz0ok996.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g2/M00/D5/5B/autohomecar__ChsEkF7pyvuAf0nuAADGhWtz0ok996.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>车身侧面，06最大的特点就是黑色的B、C柱设计，营造出悬浮式车顶的视觉效果。如果仔细观察就会发现， <span class=\"word-ser-span\" data-sercarid=\"5555\" data-word=\"领克06\">领克06</span>的C柱并不是简单的黑色处理，而是加入了菱形纹理，官方称之为“棱面机甲C柱”。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M05/2E/39/autohomecar__ChwFj17pyvyAQMnKAACyFCcYyBQ411.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M05/2E/39/autohomecar__ChwFj17pyvyAQMnKAACyFCcYyBQ411.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>此外再加上干练的腰线，06的整体效果是要比01和02更动感的。车身尺寸方面，领克06的长宽高分别为4340/1820/1625mm，轴距为2640mm。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M0A/36/F7/autohomecar__ChsEel7pyvyADscaAAC5aI_tiZM866.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M0A/36/F7/autohomecar__ChsEel7pyvyADscaAAC5aI_tiZM866.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>车尾方面，06的尾灯由原来“手枪状”的造型变为Y字型，内部构造依然是熟悉的“能量晶体”造型，视觉效果精致。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g2/M08/D5/5B/autohomecar__ChsEkF7pyvyAB40bAACqnbqYW0Q530.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g2/M08/D5/5B/autohomecar__ChsEkF7pyvyAB40bAACqnbqYW0Q530.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x384\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>可以看到尾灯处的车身宽度相比上方车窗处更宽，有一个比较健壮的车肩造型，使整体姿态更加厚重有气势。同时领克06采用双边共两出的银色排气，运动感到位。<br></p>\n" +
                " <p>内饰焕新，细节诚意满满</p>\n" +
                " <p>如果说06的外观已经让你感到很惊喜的话，坐进车内，你更会感受到新车型的诚意。06的内饰设计一改延续几款车型的家族风格，变化较大。</p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M02/36/F7/autohomecar__ChsEel7pyvyAWBlyAACdUwKoTHg900.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M02/36/F7/autohomecar__ChsEel7pyvyAWBlyAACdUwKoTHg900.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>新车中控屏采用了悬浮式的设计，空调出风口被放置在了屏幕下方。整体布局简洁，线条强调层次感。此外，新车还采用了全新样式的电子挡把，运动气息更加浓厚。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M07/32/A4/autohomecar__ChsEmF7pyv2AahRLAACEjDuv6fo629.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M07/32/A4/autohomecar__ChsEmF7pyv2AahRLAACEjDuv6fo629.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p><br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g2/M0A/0E/4F/autohomecar__ChsEml7pyv2AU9xNAABwsfwLnro575.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g2/M0A/0E/4F/autohomecar__ChsEml7pyv2AU9xNAABwsfwLnro575.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>除了造型之外，领克06内饰的做工用料也值得肯定。在日常使用经常触及的地方都采用了软性材质的包裹，造型运动的平底方向盘采用打孔的皮质包裹，握感出色。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M08/32/A4/autohomecar__ChsEmF7pyv6AH4_rAACDj9HBK3E073.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M08/32/A4/autohomecar__ChsEmF7pyv6AH4_rAACDj9HBK3E073.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p><br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M06/32/A4/autohomecar__ChsEmF7pyv-ABXJlAAB6eai31s8910.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M06/32/A4/autohomecar__ChsEmF7pyv-ABXJlAAB6eai31s8910.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>在色彩方面06的内饰也是下了功夫的，白色、灰色以及橙蓝饰板的搭配，活力有质感的同时也不落俗套。<br></p>\n" +
                " <p>空间表现值得肯定</p>\n" +
                " <p>虽然从账面数据来看，领克06的身材不是那么“强壮”，但实际的空间表现还是很令人满意的。</p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M07/32/A4/autohomecar__ChsEmF7pyv-AWObfAACyFB6BIHI620.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M07/32/A4/autohomecar__ChsEmF7pyv-AWObfAACyFB6BIHI620.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x384\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p><br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M00/32/A4/autohomecar__ChsEmF7pywCALioqAACLwsNRuxc934.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M00/32/A4/autohomecar__ChsEmF7pywCALioqAACLwsNRuxc934.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x384\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>图中体验者身高187cm，前排驾驶位调到合适坐姿，得益于天窗的因素，头部空间非常宽敞，有两拳的余量。保持坐姿不变，领克06的后排空间依然可圈可点，腿部空间一拳加一指，头部也有一拳的空间，对于这款车的车身尺寸来说空间表现值得肯定。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M09/36/F8/autohomecar__ChsEel7pywCASyQbAACfvoocwGk410.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M09/36/F8/autohomecar__ChsEel7pywCASyQbAACfvoocwGk410.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p><br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g2/M05/0E/4F/autohomecar__ChsEml7pywCAayMeAACUSGw0N24100.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g2/M05/0E/4F/autohomecar__ChsEml7pywCAayMeAACUSGw0N24100.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>领克06的座椅依然采用了拼色的设计，三种颜色的搭配使得驾舱氛围比较清新。同时前排座椅为一体式的运动座椅，有不错的包裹性和支撑性，填充物软硬适中。<br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g2/M0B/F8/E7/autohomecar__ChwFql7pywGAWFICAACinzfl9Lc879.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g2/M0B/F8/E7/autohomecar__ChwFql7pywGAWFICAACinzfl9Lc879.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p><br></p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g25/M03/36/F8/autohomecar__ChsEel7pywGAYFXtAACmt8cTehg639.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g25/M03/36/F8/autohomecar__ChsEel7pywGAYFXtAACmt8cTehg639.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>领克06的后备厢还支持4/6比例分割放倒，放倒后的空间较为平坦。美中不足在于后备厢开口较高，搬运物品时可能会有不便。<br></p>\n" +
                " <p>动力系统选择多，插混微混全都有</p>\n" +
                " <p>动力方面，领克06提供三种动力车型可供选择，分别为燃油版、插电混动版和48V微混版。</p>\n" +
                " <p></p>\n" +
                "</div>\n" +
                "<div ahe-role=\"image\" class=\"ahe__area ahe__block ahe__image\"> \n" +
                " <img class=\"indexlazy\" data-imageurl=\"http://www2.autoimg.cn/chejiahaodfs/g2/M0B/0E/4F/autohomecar__ChsEml7pywGAVLXtAACn1V-6LgU670.png\" data-original=\"//qnwww2.autoimg.cn/chejiahaodfs/g2/M0B/0E/4F/autohomecar__ChsEml7pywGAVLXtAACn1V-6LgU670.png?imageView2/2/w/752|watermark/2/text/5LiB5LiB5p2l5L6D6L2mDQrmsb3ovabkuYvlrrY=/font/5b6u6L2v6ZuF6buR/fontsize/270/fill/d2hpdGU=/dissolve/100/gravity/SouthEast/dx/5/dy/5\" src=\"//qnwww2.autoimg.cn/youchuang/g17/M14/4C/A4/autohomecar__wKjBxlmjsUKAGC85AAC6FOmz7FQ606.png?imageView2/2/w/752\" data-original-size=\"640x383\" alt=\"\" data-seriesid=\"\"> \n" +
                "</div>\n" +
                "<div ahe-role=\"text\" class=\"ahe__area ahe__block ahe__text\">\n" +
                " <p>其中燃油版车型搭载1.5T三缸发动机，最大功率177马力，峰值扭矩265牛·米，传动系统匹配7速双离合变速箱。插电混动版车型配备1.5T三缸发动机和电机组成的动力系统，其中发动机数据与燃油版一致，匹配三元锂电池组。除了上述两种车型外，领克06还将提供带有48V微混系统的车型。<br></p>\n" +
                " <p>值得一提的是，领克06将基于BMA EVO平台打造，在产品取向上相比02车型要更加注重舒适性和空间表现。</p>\n" +
                " <p>领克06的出现，进一步补齐了领克家族在紧凑型SUV市场的产品分布。至此，领克在这一细分市场已经布局了风格各异的四款车型，可见其对这一领域的重视。从产品本身来讲，06的设计相比以往的车型有所突破，视觉上能给人较强的新鲜感。通过和厂商交流，我们得知领克06将会更侧重舒适性和空间的表现，同时价格也会比较有诚意。综上所述不难看出领克06的定位精准，今后的市场表现可以期待一下。</p>\n" +
                " <p><br></p>\n" +
                "</div>";

























        Document document = Jsoup.parse(html);

        Elements elements = document.getElementsByTag("img");
        elements.forEach(element -> {
            String oldImage = element.attr("data-imageurl");
//            System.out.println(oldImage);
//            System.out.println(oldImage.substring(oldImage.lastIndexOf(".")));
            String newImage = UUID.randomUUID().toString();
            element.attr("data-imageurl",newImage);
//            try {
//                bucketManager.fetch(oldImage,BUCKET_NAME,newImage);
//
//            } catch (QiniuException e) {
//                e.printStackTrace();
//            }
        });
        String a = document.html();
        String b = document.outerHtml();
        String c = document.body().html();
        System.out.println(html);


    }

}
