package com.ruiao.car.utils;

import lombok.extern.slf4j.Slf4j;
import java.util.HashMap;
import java.util.Map;

/**
 * 创蓝短信接口
 */
@Slf4j
public class HttpSender {

    /**
     * 发送单条创蓝短信
     *
     * @param uri
     * @param account
     * @param pswd
     * @param mobiles
     * @param content
     * @return
     * @throws Exception
     */
    public static String send(String uri, String account, String pswd,
                              String mobiles, String content) throws Exception {
        return baseSend(uri, account, pswd, mobiles, content,  null, null, null, null);
    }

    /**
     * 批量发送创蓝短信
     *
     * @param uri
     * @param account
     * @param pswd
     * @param mobiles
     * @param content
     * @return
     * @throws Exception
     */
    public static String batchSend(String uri, String account, String pswd,
                                   String mobiles, String content) throws Exception {
        return baseSend(uri, account, pswd, mobiles, content,  null, null, null, null);
    }


    /**
     * 发送创蓝短信发送方法
     *
     * @param uri      地址
     * @param account  账户
     * @param pswd     密码
     * @param mobiles  手机号
     * @param content  内容
     * @param sendtime
     * @param report
     * @param extend
     * @param uid
     * @return
     * @throws Exception
     */
    private static String baseSend(String uri, String account, String pswd,
                                   String mobiles, String content,
                                   String sendtime, String report, String extend, String uid) throws Exception {
        //检查是否有代理有代理设置代理
        Map<String, Object> map = new HashMap();
        map.put("account", account);
        map.put("password", pswd);
        map.put("phone", mobiles);
        map.put("msg", content);
        map.put("sendtime", sendtime);
        map.put("report", report);
        map.put("extend", extend);
        map.put("uid", uid);
        Map headerMap = new HashMap();
        headerMap.put("Content-Type", "application/json");
        return HttpClientUtils.doPost(uri, map, headerMap);
    }
}
