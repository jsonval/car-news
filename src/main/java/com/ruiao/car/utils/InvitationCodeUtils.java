package com.ruiao.car.utils;

import java.util.Random;

/**
 * Created by jsonval on 2019/12/24.
 */
public class InvitationCodeUtils {

    public static String genInvitationCode() {
        StringBuilder str = new StringBuilder();//定义变长字符串
        Random random = new Random();
        //随机生成数字，并添加到字符串
        for (int i = 0; i < 8; i++) {
            str.append(random.nextInt(10));
        }
        return str.toString();
    }

}
