package com.ruiao.car.utils;

import org.apache.commons.codec.binary.Base64;

/**
 * Created by jsonval on 2020/5/13.
 */
public class PwdUtils {


    public static String webDec(String pwd) throws Exception {
        String reversePwd = new StringBuilder(pwd).reverse().toString();
        //先反编码base64，获取用户反转密码
        return (new Base64()).encodeToString(reversePwd.getBytes());
    }

    public static void main(String[] args)throws Exception {

        System.out.println(webDec("123456"));

    }
}
