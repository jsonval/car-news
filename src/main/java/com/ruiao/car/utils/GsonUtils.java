package com.ruiao.car.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;


public class GsonUtils {
    public static Gson get() {
        GsonBuilder builder = new GsonBuilder();
//        builder.registerTypeAdapter(Date.class, new LongDateTypeAdapter());
        Gson gson = builder.create();
        return gson;
    }

    public static String toJson(Object value) {
        return get().toJson(value);
    }

    public static <T> T fromJson(String json, Class<T> classOfT) throws JsonParseException {
        return get().fromJson(json, classOfT);
    }

    public static <T> T fromJson(String json, Type typeOfT) throws JsonParseException {
        return (T) get().fromJson(json, typeOfT);
    }

}

