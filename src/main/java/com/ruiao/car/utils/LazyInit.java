package com.ruiao.car.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LazyInit {


    public static final ExecutorService executor = Executors.newFixedThreadPool(5);


    public static ExecutorService getExecutor() {
        return executor;
    }


}
