package com.ruiao.car.utils;


import com.ruiao.car.dto.AccountDto;

public final class AccountHeaderUtil {

    private static final long serialVersionUID = -2988099637051741258L;

    private HeaderUtil headerUtil;

    private AccountDto account;

    public AccountHeaderUtil(HeaderUtil headerUtil) {
        this.headerUtil = headerUtil;
    }

    public HeaderUtil getHeaderUtil() {
        return headerUtil;
    }

    public void setHeaderUtil(HeaderUtil headerUtil) {
        this.headerUtil = headerUtil;
    }

    public AccountDto getAccount() {
        return account;
    }

    public void setAccount(AccountDto account) {
        this.account = account;
    }
}
