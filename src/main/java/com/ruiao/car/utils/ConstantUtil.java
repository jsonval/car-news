package com.ruiao.car.utils;


public class ConstantUtil {

    /**
     * expire time
     */
    public static final int ONE_MINUTE = 60;
    public static final int FIVE_MINUTE = 5 * 60;
    public static final int TEN_MINUTE = 10 * 60;
    public static final int HALF_HOUR = 30 * 60;
    public static final int ONE_HOUR = 60 * 60;
    public static final int HALF_DAY = 12 * 60 * 60;
    public static final int ONE_DAY = 24 * 60 * 60;
    public static final int THREE_DAY = 3 * 24 * 60 * 60;
    public static final int ONE_WEEK = 7 * 24 * 60 * 60;
    public static final int ONE_MONTH = 30 * 24 * 60 * 60;


    public static final int DEFAULT_PAGE_SIZE = 10;//默认分页长度
    public static final int CONSUMER_ITERATE_TIMES = 3;


    public static final int CASH_SCALE = 2;


    public static final int FUND_CASH_SCALE = 4;


    public static final String WITHDRAW_LOCK = "WITHDRAW_LOCK";


}
