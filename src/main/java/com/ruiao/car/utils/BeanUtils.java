package com.ruiao.car.utils;

import lombok.extern.slf4j.Slf4j;
import org.thymeleaf.util.ListUtils;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by jsonval on 2019/7/9.
 */
@Slf4j
public class BeanUtils {

    public static <T> T copyProperties(Object copyOb, Class<T> clazz) throws Exception {
        if (copyOb == null) {
            throw new NullPointerException();
        }
        Object ob = clazz.newInstance();
        org.springframework.beans.BeanUtils.copyProperties(copyOb, ob);
        return (T) ob;
    }

    public static void copyProperties(Object copyOb, Object returnOb) throws Exception {
        if (copyOb == null) {
            throw new NullPointerException();
        }
        org.springframework.beans.BeanUtils.copyProperties(copyOb, returnOb);
    }


    /**
     * 通过属性Get方法获取属性值
     * getAccountId,getAutoId
     *
     * @param list
     * @return
     */
    public static Set<String> getValuesByMethodName(List<Object> list, String methodName) {
        Set<String> listSet = new HashSet<>();
        if (!ListUtils.isEmpty(list)) {
            for (Object ob : list) {
                try {
                    Method getAutoIdMethod = ob.getClass().getMethod(methodName);
                    Object id = getAutoIdMethod.invoke(ob);
                    if (id != null) {
                        listSet.add(id.toString());
                    }
                } catch (Exception e) {
                    log.error("getValuesByMethodName error{}", e.getMessage());
                }
            }
        }
        return listSet;
    }
}
