package com.ruiao.car.utils;


/**
 * @author Vincent
 * @date 2018/9/3下午12:48
 * @Description:
 */
public class AccountThreadLocal {

    private AccountThreadLocal(){

    }

    private static final ThreadLocal<AccountHeaderUtil> threadLocal = new ThreadLocal<AccountHeaderUtil>();

    public static void set(AccountHeaderUtil accountHeader){
        threadLocal.set(accountHeader);
    }


    public static AccountHeaderUtil get(){
        return threadLocal.get();
    }

    public static HeaderUtil header(){
        if( threadLocal.get() == null ){
            return null;
        }
        return threadLocal.get().getHeaderUtil();
    }


    public static void remove(){
        threadLocal.remove();
    }


}

