package com.ruiao.car.utils;

import java.util.UUID;

/**
 * Created by jsonval on 2019/11/19.
 */
public class CodeUtils {
    public CodeUtils() {
    }

    public static String getCode(int length) {
        length = length >= 32?32:length;
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, length);
    }
}
