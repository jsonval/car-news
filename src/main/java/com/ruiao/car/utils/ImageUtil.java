package com.ruiao.car.utils;

import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author yinpingliu
 * @name ImageUtil
 * @time: 2020/6/18 上午10:09
 * @description: No Description
 */
public class ImageUtil {

    public static String generateImageName(MultipartFile file){
        String realName = file.getOriginalFilename();
        String fileName = generateImageNamePre();
        fileName = fileName+ realName.substring(realName.lastIndexOf("."));
        return fileName;
    }

    public static String generateImageNamePre(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(new Date())+"/"+ UUID.randomUUID().toString().replaceAll("-","").substring(0,30);
    }


}
