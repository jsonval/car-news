package com.ruiao.car.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jsonval on 2019/3/18.
 */
public class MD5Utils {

    /**
     * @param str
     * @return
     */
    public static String getMD5String(String str) {
        MessageDigest md = null;
        StringBuffer sb = new StringBuffer();
        byte[] tmpByte = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(str.getBytes());
        tmpByte = md.digest();
        for (int i = 0; i < tmpByte.length; ++i) {
            if ((tmpByte[i] & 0xFF) < 16) {
                sb.append("0");
            }
            sb.append(Long.toString(tmpByte[i] & 0xFF, 16));
        }
        return sb.toString();
    }

}
