package com.ruiao.car.dto;

import com.ruiao.car.enums.AppIdEnum;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by jsonval on 2020/6/12.
 */
@Data
@Builder
public class WormDataDto {
    AppIdEnum appId;
    String channelId;
    String sourceId;
    List<String> coverImages;
    String title;
    String absContent;
    String author;
    Date publishTime;
    String origin;
    String content;
    private String contentUrl;
}
