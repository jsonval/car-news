package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 消息推送
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-07-13
 */
@Data
@ApiModel(value = "MessagePushDto",description = "消息")
public class MessagePushDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 数据
     */
    private String data;

    /**
     * 0-未读，1-已读
     */
    private Integer receipt;



    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 更新日期
     */
    private Date updateDate;

}
