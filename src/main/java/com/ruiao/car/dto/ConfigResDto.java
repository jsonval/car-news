package com.ruiao.car.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruiao.car.entity.ConfigEntity;
import com.ruiao.car.enums.ConfigTypeEnum;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Data
public class ConfigResDto<T>{

    @JsonIgnore
    private Long id;
    private List<T> paramValue;
    private Integer  configVersion;

    @JsonIgnore
    private Map<String,LinkedHashMap> dynamicParam;

    @JsonIgnore
    private ConfigTypeEnum configType;

    public ConfigResDto() {
    }

    public ConfigResDto(ConfigEntity<List<T>> configDto) {
        if( configDto == null){
            return ;
        }
        this.id = configDto.getId();
        this.paramValue = configDto.getParamValue();
        this.configVersion = configDto.getConfigVersion();
        this.dynamicParam = configDto.getDynamicParam();
        this.configType = configDto.getConfigType();
    }
}
