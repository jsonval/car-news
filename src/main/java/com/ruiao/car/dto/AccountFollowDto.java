package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yinpingliu
 * @name ArticlesPraiseDto
 * @time: 2020/6/16 上午11:01
 * @description: No Description
 */
@ApiModel(value = "AccountFollowDto",description = "关注信息")
@Data
public class AccountFollowDto implements Serializable {

    @ApiModelProperty(value = "用户ID")
    private Long accountId;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

}
