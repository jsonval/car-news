package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="AppVersionDto",description="app版本请求数据")
public class AppVersionDto {

	@ApiModelProperty(value="平台(ios,android)",name="platform",example="ios")
	private String platform;
	
	@ApiModelProperty(value="carNews,",name="appId",example="carNews")
	private String appId;
}


