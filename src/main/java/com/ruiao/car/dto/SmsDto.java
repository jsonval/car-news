package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "SmsDto",description = "发送短信")
public class SmsDto {

	 @ApiModelProperty(value = "用户ID(1注册，2登录，3修改密码，4其他)",name = "1注册，2登录，3修改密码，4其他",required = true)
	private String smsType; 
	
	 @ApiModelProperty(value = "手机号码",name = "phone",required = true)
	private String phone;
	
}
