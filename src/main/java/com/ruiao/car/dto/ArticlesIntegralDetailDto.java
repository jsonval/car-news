package com.ruiao.car.dto;

import com.ruiao.car.entity.Account;
import com.ruiao.car.entity.Articles;
import com.ruiao.car.enums.AccountAttachEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yinpingliu
 * @name ArticlesPraiseDto
 * @time: 2020/6/16 上午11:01
 * @description: No Description
 */
@ApiModel(value = "ArticlesIntegralDetailDto",description = "用户积分")
@Data
public class ArticlesIntegralDetailDto implements Serializable {

    @ApiModelProperty(value = "记录值")
    private Integer attachValue;

    @ApiModelProperty(value = "文章信息")
    private Articles articles;

    @ApiModelProperty(value = "1-文章 2-主动转赠 3-接受转赠")
    private Integer sourceType;

    @ApiModelProperty(value = "来源ID")
    private String sourceId;

    @ApiModelProperty(value = "时间")
    private Date createDate;

    @ApiModelProperty(value = "文案展示")
    private String sourceInfo;

    public String getSourceInfo() {
        if(sourceType!=null){
            if(sourceType.intValue() == AccountAttachEnum.ARTICLES_INTEGRAL.getSourceType().intValue()){
                return "阅读资讯";
            }else if(sourceType.intValue() == AccountAttachEnum.GIVE_INTEGRAL.getSourceType().intValue()){
                return "赠送扣除";
            }else if(sourceType.intValue() == AccountAttachEnum.GIVED_INTEGRAL.getSourceType().intValue()){
                return "赠送获得";
            }else if(sourceType.intValue() == AccountAttachEnum.INCENTIVE_VIDEO_INTEGRAL.getSourceType().intValue()){
                return "观看视频";
            }else if(sourceType.intValue() == AccountAttachEnum.SIGN_IN_DAY_1.getSourceType().intValue()){
                return "每日签到";
            }
        }
        return sourceInfo;
    }
}
