package com.ruiao.car.dto;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author yinpingliu
 * @name ArticlesPraiseDto
 * @time: 2020/6/16 上午11:01
 * @description: No Description
 */
@ApiModel(value = "ArticlesIntegralTotalDto",description = "用户积分")
@Data
public class ArticlesIntegralTotalDto implements Serializable {

    @ApiModelProperty(value = "分页信息")
    private IPage<ArticlesIntegralDetailDto> articlesIntegralPageInfo;

    @ApiModelProperty(value = "用户积分")
    private BigDecimal integralTotal;

    @ApiModelProperty(value = "MLT地址")
    private String mltAddress;


}
