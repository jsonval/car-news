package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
@ApiModel(value = "UserDto",description = "用户操作相关")
public class UserDto {

	private Long id;
	 @ApiModelProperty(value = "头像",name = "avatar")
	private String avatar;
	 @ApiModelProperty(value = "昵称",name = "nickName")
	private String nickName;
	private String appId;
	 @ApiModelProperty(value = "手机号码",name = "phone")
	private String phone;
	 @ApiModelProperty(value = "密码",name = "password")
	private String password;
	 @ApiModelProperty(value = "验证码",name = "verificationCode")
	private String verificationCode;
	

	
}
