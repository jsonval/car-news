package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author yinpingliu
 */
@Data
@ApiModel(value = "UserUpdateDto",description = "用户操作相关")
public class UserUpdateDto {

	 @ApiModelProperty(value = "头像",name = "avatar")
	private String avatar;

	 @ApiModelProperty(value = "昵称",name = "nickName")
	private String nickName;

	 @ApiModelProperty(value = "介绍",name = "introduction")
	 private String introduction;

}
