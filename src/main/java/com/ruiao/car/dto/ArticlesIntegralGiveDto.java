package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author yinpingliu
 * @name ArticlesPraiseDto
 * @time: 2020/6/16 上午11:01
 * @description: No Description
 */
@ApiModel(value = "ArticlesIntegralGiveDto",description = "赠送积分")
@Data
public class ArticlesIntegralGiveDto implements Serializable {

    @ApiModelProperty(value = "MLT地址")
    @NotEmpty(message = "对方地址不能为空")
    private String mltAddress;

    @ApiModelProperty(value = "赠送数量")
    @Min(value = 10,message = "赠送数量不能少于10")
    @NotNull(message = "赠送数量不能为空")
    private BigDecimal integral;


}
