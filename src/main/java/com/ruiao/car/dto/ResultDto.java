package com.ruiao.car.dto;


import com.ruiao.car.enums.IResultEnum;
import com.ruiao.car.enums.ResultEnum;
import lombok.Data;

import java.io.Serializable;

@Data
public class ResultDto<T> implements Serializable{

    private Integer errCode;
    private String errMsg;
    private T data;

    public ResultDto() {
    }

    public ResultDto(Integer code, String msg) {
        this.errCode = code;
        this.errMsg = msg;
    }

    public ResultDto(Integer code, String msg, T data) {
        this.errCode = code;
        this.errMsg = msg;
        this.data = data;
    }

    public ResultDto(Integer code, String msg, String... args) {
        this.errCode = code;
        if (args.length > 0) {
            for (String arg : args) {
                msg = msg.replaceFirst("%s", arg);
            }
        }
        this.errMsg = msg;
    }

    public ResultDto(IResultEnum resultEnum) {
        this.errCode = resultEnum.getErrCode();
        this.errMsg = resultEnum.getErrMsg();
    }

    public ResultDto(IResultEnum resultEnum, String... args) {
        this.errCode = resultEnum.getErrCode();
        String msg = resultEnum.getErrMsg();
        if (args.length > 0) {
            for (String arg : args) {
                msg = msg.replaceFirst("%s", arg);
            }
        }
        this.errMsg = msg;
    }

    public ResultDto(IResultEnum resultEnum, T data) {
        this.errCode = resultEnum.getErrCode();
        this.errMsg = resultEnum.getErrMsg();
        this.data = data;
    }

    public static <T> ResultDto<T> get(IResultEnum e) {

        if (null == e) {
            return new ResultDto(ResultEnum.SUCCESS);
        }

        return new ResultDto(e);
    }


}
