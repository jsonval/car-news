package com.ruiao.car.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;

/**
 * @author yinpingliu
 * @name CjhWebDto
 * @time: 2020/6/18 上午10:54
 * @description: No Description
 */
@Getter
@Setter
@Builder
public class CjhWebDto {
    private String listUrl;
    private String channelId;
    private String detailUrl;

    @Tolerate
    public CjhWebDto() {
    }
}
