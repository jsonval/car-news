package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author yinpingliu
 * @name ArticlesPraiseDto
 * @time: 2020/6/16 上午11:01
 * @description: No Description
 */
@ApiModel(value = "ArticlesIntegralDto",description = "用户积分")
@Data
public class ArticlesIntegralDto implements Serializable {

    @ApiModelProperty(value = "文章ID")
    @NotEmpty(message = "文章ID不能为空")
    private String articlesId;


}
