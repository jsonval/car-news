package com.ruiao.car.dto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.thymeleaf.util.StringUtils;

import com.ruiao.car.constant.Constant;

import java.math.BigDecimal;

/**
 * @author Vincent.Pei
 * @date 2019/2/22下午2:42
 * @Description: 用户dto
 */

@Data
@ApiModel(value = "AccountDto",description = "用户信息")
public class AccountDto {
	
	private Long id;
	@ApiModelProperty(value = "图像")
	private String avatar;
	@ApiModelProperty(value = "昵称")
	private String nickName;
	private String appId;
	@ApiModelProperty(value = "手机号")
	private String mobilePhone;
    
    /**
     * 实名认证
     */
    private Boolean cerStatus;
    private String level;
    private String invitationCode;
    private String secretKey;

    /**
     * 性别
     */
    private Byte gender;

    private String wxAccount;

    /**
     * 是否有兑换密码
     */
    private boolean hasExchangePwd;


    /**
     * 用户状态 show正常 del 删除 black拉黑 frozen 冻结
     */
    private String accountStatus;

    private Boolean invitation;

	private String introduction;
    
    private String token;

    @ApiModelProperty(value = "用户积分")
    private BigDecimal integralTotal;

    public AccountDto() {

    }

    public void setExchangePwd(String exchangePwd) {
        if (StringUtils.isEmpty(exchangePwd)) {
            setHasExchangePwd(false);
        } else {
            setHasExchangePwd(true);
        }
    }
    public void setNickName(String nickName) {
		if (StringUtils.isEmpty(nickName)) {
			if (!StringUtils.isEmpty(mobilePhone)) {
				this.nickName = blurPhone(mobilePhone);
			}
		} else {
			this.nickName = nickName;
		}
	}

	public void setAvatar(String avatar) {
		this.avatar = StringUtils.isEmpty(avatar) ? Constant.default_avatar : avatar;
	}

	public void setMobilePhone(String mobilePhone) {
		if (!StringUtils.isEmpty(mobilePhone)) {
			this.mobilePhone = blurPhone(mobilePhone);
		}
	}

	/**
	 * 手机号脱敏筛选正则
	 */
	public static final String PHONE_BLUR_REGEX = "(\\d{3})\\d{4}(\\d{4})";

	/**
	 * 手机号脱敏替换正则
	 */
	public static final String PHONE_BLUR_REPLACE_REGEX = "$1****$2";

	public static final String blurPhone(String phone) {

		return phone.replaceAll(PHONE_BLUR_REGEX, PHONE_BLUR_REPLACE_REGEX);
	}

}

