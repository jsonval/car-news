package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author yinpingliu
 * @name ArticlesPraiseDto
 * @time: 2020/6/16 上午11:01
 * @description: No Description
 */
@ApiModel(value = "ArticlesCommentDto",description = "评论信息")
@Data
public class ArticlesCommentDto implements Serializable {

    /**
     * 1-文章，2-评论
     */
    @NotNull(message = "宿主类型不能为空")
    @ApiModelProperty(value = "1-文章，2-评论")
    private Integer hostType;

    /**
     * 文章的自增ID 或者 评论的自增ID
     */
    @NotEmpty(message = "宿主ID不能为空")
    @ApiModelProperty(value = "文章的自增ID 或者 评论的自增ID")
    private String hostId;

    /**
     * 评论内容
     */
    @NotEmpty(message = "评论内容不能为空")
    @ApiModelProperty(value = "评论内容")
    private String commentContent;

}
