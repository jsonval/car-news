package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 消息推送
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-07-13
 */
@Data
@ApiModel(value = "MessagePushQueryDto",description = "消息")
public class MessagePushQueryDto implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "上一次的UID")
    private Long lastUid = 0L;

    private Integer limit=10;

}
