package com.ruiao.car.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author yinpingliu
 * @name SignInListDto
 * @time: 2020/6/16 上午11:01
 * @description: No Description
 */
@ApiModel(value = "signInListDto",description = "签到列表信息")
@Data
@Builder
public class SignInListDto implements Serializable {

    @ApiModelProperty(value = "第几天")
    private int day;

    @ApiModelProperty(value = "对应积分值")
    private BigDecimal attachValue;

    @ApiModelProperty(value = "true获取 false未获取")
    private Boolean isObtain;

    @Tolerate
    public SignInListDto() {
    }
}
