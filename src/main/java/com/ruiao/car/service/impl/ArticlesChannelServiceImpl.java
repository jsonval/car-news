package com.ruiao.car.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruiao.car.dto.AccountDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Articles;
import com.ruiao.car.entity.ArticlesChannel;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.ArticlesChannelMapper;
import com.ruiao.car.service.IArticlesChannelService;
import com.ruiao.car.service.IArticlesOperateService;
import com.ruiao.car.service.IArticlesService;
import com.ruiao.car.utils.AccountThreadLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台内容频道 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Service
public class ArticlesChannelServiceImpl extends ServiceImpl<ArticlesChannelMapper, ArticlesChannel> implements IArticlesChannelService {

    @Autowired
    private IArticlesService articlesService;

    @Autowired
    private IArticlesOperateService articlesOperateService;


    @Override
    public ResultDto feedList(Page page, Long channelId) {
        String appId = AccountThreadLocal.get().getHeaderUtil().getAppId();
        IPage<ArticlesChannel> pageList = this.getBaseMapper().selectPage(page,
                Wrappers.<ArticlesChannel>query().lambda()
                        .eq(ArticlesChannel::getAppId, appId).eq(ArticlesChannel::getChannelId, channelId)
                        .orderByDesc(ArticlesChannel::getPublishTime)
        );
        for (ArticlesChannel articlesChannel : pageList.getRecords()) {
            Articles articles = articlesService.getBaseMapper().selectById(articlesChannel.getArticlesId());
            if (articles != null) {
                articlesOperateService.rebuildOp(articles);

                articlesChannel.setArticles(articles);

            }
        }
        return new ResultDto(ResultEnum.SUCCESS, pageList);

    }

}
