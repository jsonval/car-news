package com.ruiao.car.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruiao.car.entity.Articles;
import com.ruiao.car.entity.ArticlesOperate;
import com.ruiao.car.mapper.ArticlesOperateMapper;
import com.ruiao.car.service.IArticlesOperateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 内容基础数据 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-17
 */
@Service
public class ArticlesOperateServiceImpl extends ServiceImpl<ArticlesOperateMapper, ArticlesOperate> implements IArticlesOperateService {


    public void rebuildOp(Articles articles) {
        if (articles != null) {
            ArticlesOperate articlesOperate = getBaseMapper().selectOne(Wrappers.<ArticlesOperate>query().lambda().eq(ArticlesOperate::getArticlesId, articles.getId()));
            if (articlesOperate == null) {
                articlesOperate = new ArticlesOperate(articles.getId());
                save(articlesOperate);
            }
            articles.rebuildPv(articlesOperate);

        }
    }


    public void visit(Articles articles) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (articles != null) {
                    ArticlesOperate articlesOperate = getBaseMapper().selectOne(Wrappers.<ArticlesOperate>query().lambda().eq(ArticlesOperate::getArticlesId, articles.getId()));
                    if (articlesOperate != null) {
                        articlesOperate.setPv(articlesOperate.getPv() + 1);
                        updateById(articlesOperate);
                    } else {
                        articlesOperate = new ArticlesOperate(articles.getId());
                        save(articlesOperate);
                    }
                }
            }
        }).start();

    }

}
