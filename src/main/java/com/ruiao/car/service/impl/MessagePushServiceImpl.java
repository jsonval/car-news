package com.ruiao.car.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruiao.car.dto.MessagePushDto;
import com.ruiao.car.dto.MessagePushQueryDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.MessagePush;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.MessagePushMapper;
import com.ruiao.car.service.IMessagePushService;
import com.ruiao.car.utils.AccountHeaderUtil;
import com.ruiao.car.utils.AccountThreadLocal;
import com.ruiao.car.utils.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 消息推送 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-07-13
 */
@Service
public class MessagePushServiceImpl extends ServiceImpl<MessagePushMapper, MessagePush> implements IMessagePushService {

    @Override
    public ResultDto<List<MessagePushDto>> listByLastUid(MessagePushQueryDto messagePushQueryDto) {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        List<Long> accountIdList = new ArrayList<>();
        accountIdList.add(0L);
        if(null!=accountThreadLocal.getAccount() && null!=accountThreadLocal.getAccount().getId()){
            accountIdList.add(accountThreadLocal.getAccount().getId());
        }

        List<MessagePush> List = list(new QueryWrapper<MessagePush>().lambda()
                .in(MessagePush::getAccountId,accountIdList).lt(MessagePush::getId,messagePushQueryDto.getLastUid())
                .eq(MessagePush::getMessageStatus,1).eq(MessagePush::getReceipt,0).orderByDesc(MessagePush::getId));

        List<MessagePushDto> messagePushDtoList = new ArrayList<>(List.size());

        list().forEach(messagePush -> {
            MessagePushDto messagePushDto = new MessagePushDto();
            try {
                BeanUtils.copyProperties(messagePush,messagePushDto);
            } catch (Exception e) {
                e.printStackTrace();
            }
            messagePushDtoList.add(messagePushDto);
        });

        return new ResultDto(ResultEnum.SUCCESS, messagePushDtoList);
    }

    @Override
    public ResultDto<Boolean> readMessage(Long id) {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        MessagePush messagePush = getById(id);
        if(null == messagePush){
            return new ResultDto(ResultEnum.SUCCESS, "消息不存在");
        }

        if(messagePush.getAccountId().intValue() == accountThreadLocal.getAccount().getId()){
            messagePush.setReceipt(1);
            messagePush.setUpdateDate(new Date());
        }

        return new ResultDto(ResultEnum.SUCCESS, this.updateById(messagePush));
    }

    @Override
    public ResultDto<Integer> newMessageCount(MessagePushQueryDto messagePushQueryDto) {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        Integer newMessageCount = count(new QueryWrapper<MessagePush>().lambda().eq(MessagePush::getAccountId,0).gt(MessagePush::getId,messagePushQueryDto.getLastUid())
                .eq(MessagePush::getMessageStatus,1).eq(MessagePush::getReceipt,0));
        if(null!=accountThreadLocal.getAccount() && null!=accountThreadLocal.getAccount().getId()){
            Integer personCount = count(new QueryWrapper<MessagePush>().lambda().eq(MessagePush::getAccountId,accountThreadLocal.getAccount().getId())
                    .eq(MessagePush::getMessageStatus,1).eq(MessagePush::getReceipt,0));
            newMessageCount = newMessageCount + personCount;
        }

        return new ResultDto(ResultEnum.SUCCESS, newMessageCount);
    }
}
