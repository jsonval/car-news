package com.ruiao.car.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Channel;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.ChannelMapper;
import com.ruiao.car.service.IChannelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 平台频道 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Service
public class ChannelServiceImpl extends ServiceImpl<ChannelMapper, Channel> implements IChannelService {

    @Override
    public ResultDto channelList() {
        List<Channel> list = this.getBaseMapper().selectList(Wrappers.<Channel>query().lambda()
                .eq(Channel::getChannelStatus, true)
                .orderByDesc(Channel::getChannelSort)
        );
        return new ResultDto(ResultEnum.SUCCESS, list);
    }
}
