package com.ruiao.car.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.*;
import com.ruiao.car.enums.AccountAttachEnum;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.ArticlesMapper;
import com.ruiao.car.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruiao.car.utils.AccountHeaderUtil;
import com.ruiao.car.utils.AccountThreadLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台内容 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Service
public class ArticlesServiceImpl extends ServiceImpl<ArticlesMapper, Articles> implements IArticlesService {

    @Autowired
    private IArticlesContentService articlesContentService;

    @Autowired
    private IArticlesOperateService articlesOperateService;

    @Autowired
    private IAccountAttachDetailService accountAttachDetailService;

    @Autowired
    private IPraiseService praiseService;

    @Autowired
    private ICollectService collectService;

    @Override
    public ResultDto detail(Long id) {
        Articles articles = getById(id);

        articlesOperateService.visit(articles);

        if (articles != null) {
            ArticlesContent articlesContent = articlesContentService.getBaseMapper().selectOne(Wrappers.<ArticlesContent>query().lambda().eq(ArticlesContent::getArticlesId, id));
            articles.setArticlesContent(articlesContent);
            articlesOperateService.rebuildOp(articles);

            articles = getArticlesOperateInfo(articles);

        }

        return new ResultDto(ResultEnum.SUCCESS, articles);
    }

    @Override
    public Articles getArticlesOperateInfo(Articles articles) {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();

        if(null!=accountThreadLocal.getAccount() && null!=accountThreadLocal.getAccount().getId()){
            if(accountAttachDetailService.count(
                    new QueryWrapper<AccountAttachDetail>().lambda()
                            .eq(AccountAttachDetail::getAccountId,accountThreadLocal.getAccount().getId())
                            .eq(AccountAttachDetail::getAttachType,AccountAttachEnum.ARTICLES_INTEGRAL.getAttachType())
                            .eq(AccountAttachDetail::getSourceId,articles.getId())
                            .eq(AccountAttachDetail::getSourceType,AccountAttachEnum.ARTICLES_INTEGRAL.getSourceType())
                            .eq(AccountAttachDetail::getStatus,1))>0){
                articles.setIsIntegral(1);
            }

            if(praiseService.count(new QueryWrapper<Praise>()
                    .lambda().eq(Praise::getArticlesId,articles.getId())
                    .eq(Praise::getAccountId,accountThreadLocal.getAccount().getId()).eq(Praise::getPraiseStatus,1))>0){
                articles.setIsPraise(1);
            }
            if(collectService.count(new QueryWrapper<Collect>()
                    .lambda().eq(Collect::getArticlesId,articles.getId())
                    .eq(Collect::getAccountId,accountThreadLocal.getAccount().getId()).eq(Collect::getCollectStatus,1))>0){
                articles.setIsCollect(1);
            }

        }

        return articles;
    }


    //    public Integer getVpv() {
//        if (createTime == null) {
//            return 0;
//        }
//        if (DateUtils.diffHour(createTime, new Date()) >= 1) {
//            Long addnum = Long.parseLong(autoId) % 200 + 300;//获取0-200的值
//            return vpv + addnum.intValue();
//        }
//        return vpv;
//    }
}
