package com.ruiao.car.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruiao.car.dto.AccountFollowDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Account;
import com.ruiao.car.entity.Follow;
import com.ruiao.car.entity.Praise;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.FollowMapper;
import com.ruiao.car.service.IAccountService;
import com.ruiao.car.service.IFollowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruiao.car.utils.AccountHeaderUtil;
import com.ruiao.car.utils.AccountThreadLocal;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 关注 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@Service
public class FollowServiceImpl extends ServiceImpl<FollowMapper, Follow> implements IFollowService {

    @Autowired
    private IAccountService accountService;

    @Override
    public ResultDto addFollow(AccountFollowDto dto) {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();

        if(null == dto.getAccountId()){
            Account account = accountService.getOne(new QueryWrapper<Account>().lambda().eq(Account::getNickName,dto.getNickName()));
            if(null == account){
                account = Account.builder()
                        .nickName(dto.getNickName()).appId(accountThreadLocal.getHeaderUtil().getAppId())
                        .regTime(new Date()).createTime(new Date()).build();
                accountService.save(account);
            }
            dto.setAccountId(account.getId());
        }

        Follow follow = this.getOne(new QueryWrapper<Follow>().lambda()
                .eq(Follow::getFollowAccountId,dto.getAccountId())
                .eq(Follow::getAccountId,accountThreadLocal.getAccount().getId())
                .eq(Follow::getFollowStatus,1));
        if(follow == null){
            follow = Follow.builder()
                    .appId(accountThreadLocal.getHeaderUtil().getAppId())
                    .accountId(accountThreadLocal.getAccount().getId())
                    .accountName(accountThreadLocal.getAccount().getNickName())
                    .accountAvatar(accountThreadLocal.getAccount().getAvatar())
                    .followAccountId(dto.getAccountId())
                    .build();
            this.save(follow);
            return ResultDto.get(ResultEnum.SUCCESS);
        }
        return new ResultDto(ResultEnum.SUCCESS.getErrCode(),"已关注");
    }

    @Override
    public ResultDto deleteFollow(AccountFollowDto dto) {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        Follow follow = this.getOne(new QueryWrapper<Follow>().lambda()
                .eq(Follow::getFollowAccountId,dto.getAccountId())
                .eq(Follow::getAccountId,accountThreadLocal.getAccount().getId())
                .eq(Follow::getFollowStatus,1));
        if(null!=follow){
            follow.setUpdateDate(new Date());
            follow.setFollowStatus(0);
            this.updateById(follow);
        }
        return ResultDto.get(ResultEnum.SUCCESS);
    }
}
