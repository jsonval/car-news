package com.ruiao.car.service.impl;

import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.service.IAiService;
import com.ruiao.car.service.RedisService;
import com.ruiao.car.utils.Base64Util;
import com.ruiao.car.utils.GsonUtils;
import com.ruiao.car.utils.HttpUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.net.URLEncoder;
import java.util.Map;

/**
 * @author yinpingliu
 * @name AiServiceImpl
 * @time: 2020/7/3 下午3:04
 * @description: No Description
 */
@Service
public class AiServiceImpl implements IAiService {

    @Resource
    private RestTemplate restTemplate;
    @Resource
    private RedisService redisService;


    //https://ai.baidu.com/ai-doc/VEHICLE/fk3hb3f5w


    public static final String ACCESS_TOKEN_LINK = "https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials" +
            "&client_id=XdKUOMv4bQeZuQ207qGrVGmF" +
            "&client_secret=1XbIT9Kqno3LbaC3GNCvYtXhiXAdhdg3";

    @Override
    public String getAccessToken(){
        String accessToken = redisService.get("ai_access_token",String.class);
        if(StringUtils.isEmpty(accessToken)){
            try {
                ResponseEntity<Map> responseEntity = restTemplate.postForEntity(ACCESS_TOKEN_LINK,null,Map.class);
                accessToken = responseEntity.getBody().get("access_token").toString();
                redisService.set("ai_access_token",accessToken,2592000-600);
            }catch (Exception e){
                e.printStackTrace();
                return "";
            }

        }
        return accessToken;
    }

    @Override
    public ResultDto carModel(MultipartFile file) {
        String url = "https://aip.baidubce.com/rest/2.0/image-classify/v1/car";
        try {
            String imgStr = Base64Util.encode(file.getBytes());
            String imgParam = URLEncoder.encode(imgStr, "UTF-8");

            String param = "image=" + imgParam + "&top_num=" + 5 +"&baike_num="+5;

            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            String accessToken = getAccessToken();

            String result = HttpUtil.post(url, accessToken, param);

            return new ResultDto(ResultEnum.SUCCESS,GsonUtils.fromJson(result,Map.class));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResultDto(ResultEnum.NETWORK_ERROR);

    }

    @Override
    public ResultDto carDamage(MultipartFile file) {
        String url = "https://aip.baidubce.com/rest/2.0/image-classify/v1/vehicle_damage";
        try {
            String imgStr = Base64Util.encode(file.getBytes());
            String imgParam = URLEncoder.encode(imgStr, "UTF-8");

            String param = "image=" + imgParam;

            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            String accessToken = getAccessToken();

            String result = HttpUtil.post(url, accessToken, param);

            return new ResultDto(ResultEnum.SUCCESS,GsonUtils.fromJson(result,Map.class));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResultDto(ResultEnum.NETWORK_ERROR);
    }


}
