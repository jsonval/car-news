package com.ruiao.car.service.impl;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruiao.car.dto.AppVersionDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.AppVersion;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.AppVersionMapper;
import com.ruiao.car.service.IAppVersionService;
import com.ruiao.car.utils.AccountThreadLocal;
import com.ruiao.car.utils.HeaderUtil;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class AppVersionServiceImpl extends ServiceImpl<AppVersionMapper, AppVersion> implements IAppVersionService{

	@SuppressWarnings("unchecked")
	@Override
	public ResultDto<AppVersion> selectAppVersion(AppVersionDto appVersion) {
		log.info("====appVersion===" + appVersion.getPlatform());
		HeaderUtil headerUtil = AccountThreadLocal.get().getHeaderUtil();
		String appId = headerUtil.getAppId();
		QueryWrapper queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("platform", appVersion.getPlatform());
		queryWrapper.eq("app_id", appId);
		queryWrapper.orderByDesc("create_time");
		queryWrapper.last(" limit 1 ");
		AppVersion version = this.getOne(queryWrapper);
		if(version != null) {
			return new ResultDto(ResultEnum.SUCCESS, version);
		}
		return null;
	}

	

}
