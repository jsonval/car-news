package com.ruiao.car.service.impl;

import com.ruiao.car.entity.AccountAuth;
import com.ruiao.car.mapper.AccountAuthMapper;
import com.ruiao.car.service.IAccountAuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户登录auth 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Service
public class AccountAuthServiceImpl extends ServiceImpl<AccountAuthMapper, AccountAuth> implements IAccountAuthService {

}
