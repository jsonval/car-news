package com.ruiao.car.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.AccountAttachDetail;
import com.ruiao.car.entity.SignIn;
import com.ruiao.car.entity.SignInWeekStatis;
import com.ruiao.car.enums.AccountAttachEnum;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.SignInMapper;
import com.ruiao.car.service.IAccountAttachService;
import com.ruiao.car.service.ISignInService;
import com.ruiao.car.service.ISignInWeekStatisService;
import com.ruiao.car.utils.AccountHeaderUtil;
import com.ruiao.car.utils.AccountThreadLocal;
import com.ruiao.car.utils.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <p>
 * 签到 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-08-11
 */
@Service
public class SignInServiceImpl extends ServiceImpl<SignInMapper, SignIn> implements ISignInService {

    @Resource
    private IAccountAttachService accountAttachService;
    @Resource
    private ISignInWeekStatisService signInWeekStatisService;

    @Override
    public ResultDto addSignIn() {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        if(this.count(new QueryWrapper<SignIn>().lambda().
                eq(SignIn::getSignInDate,DateUtils.formatDate(new Date(),DateUtils.DATE_FORMAT_DEFAULT))
                .eq(SignIn::getStatus,1).eq(SignIn::getAccountId,accountThreadLocal.getAccount().getId()))>0){
            return new ResultDto(ResultEnum.REPEAT_SUBMISSION.getErrCode(),"已签到");
        }
        SignIn signIn = SignIn.builder()
                        .accountId(accountThreadLocal.getAccount().getId())
                        .appId(accountThreadLocal.getAccount().getAppId())
                        .signInDate(DateUtils.formatDate(new Date(),DateUtils.DATE_FORMAT_DEFAULT)).build();
        this.save(signIn);

        SignInWeekStatis signInWeekStatis = signInWeekStatisService.getOne(
                new QueryWrapper<SignInWeekStatis>().lambda().eq(SignInWeekStatis::getAccountId,accountThreadLocal.getAccount().getId()));
        if(null == signInWeekStatis){
            signInWeekStatis = SignInWeekStatis.builder().accountId(signIn.getAccountId()).appId(signIn.getAppId()).signCycleCount(1).signInDateLast(signIn.getSignInDate()).build();
            signInWeekStatisService.save(signInWeekStatis);
        }else {
            if(7 <= signInWeekStatis.getSignCycleCount().intValue()){
                signInWeekStatis.setSignCycleCount(1);
            }else {
                if(DateUtils.formatDate(DateUtils.addDay(new Date(),-1),DateUtils.DATE_FORMAT_DEFAULT).equals(signInWeekStatis.getSignInDateLast())){
                    signInWeekStatis.setSignCycleCount(signInWeekStatis.getSignCycleCount()+1);
                }else {
                    signInWeekStatis.setSignCycleCount(1);
                }
            }
            signInWeekStatis.setSignInDateLast(signIn.getSignInDate());
            signInWeekStatisService.updateById(signInWeekStatis);
        }

        AccountAttachEnum accountAttachEnum = getAttachByDay(getUserSignInDayLevelNum());

        AccountAttachDetail accountAttachDetail = AccountAttachDetail.builder()
                .attachType(accountAttachEnum.getAttachType())
                .attachValue(accountAttachEnum.getAttachValue())
                .sourceType(accountAttachEnum.getSourceType())
                .build();
        accountAttachService.saveArticlesIntegral(accountAttachDetail);

        return new ResultDto(ResultEnum.SUCCESS.getErrCode(),"签到成功:获取积分"+accountAttachDetail.getAttachValue(),accountAttachDetail.getAttachValue());
    }

    @Override
    public Integer getUserSignInDayLevelNum(){
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        Integer signDayLevel = 1;
        SignInWeekStatis signInWeekStatis = signInWeekStatisService.getOne(
                new QueryWrapper<SignInWeekStatis>().lambda().eq(SignInWeekStatis::getAccountId,accountThreadLocal.getAccount().getId()));
        if(null!=signInWeekStatis){
            signDayLevel = signInWeekStatis.getSignCycleCount();
        }
        return signDayLevel;
    }

    @Override
    public AccountAttachEnum getAttachByDay(Integer day){
        if(day == null){
            return AccountAttachEnum.SIGN_IN_DAY_1;
        }
        switch (day){
            case 1:
                return AccountAttachEnum.SIGN_IN_DAY_1;
            case 2:
                return AccountAttachEnum.SIGN_IN_DAY_2;
            case 3:
                return AccountAttachEnum.SIGN_IN_DAY_3;
            case 4:
                return AccountAttachEnum.SIGN_IN_DAY_4;
            case 5:
                return AccountAttachEnum.SIGN_IN_DAY_5;
            case 6:
                return AccountAttachEnum.SIGN_IN_DAY_6;
            case 7:
                return AccountAttachEnum.SIGN_IN_DAY_7;
            default:
                return AccountAttachEnum.SIGN_IN_DAY_1;
        }
    }


}
