package com.ruiao.car.service.impl;

import com.ruiao.car.entity.ArticlesContent;
import com.ruiao.car.mapper.ArticlesContentMapper;
import com.ruiao.car.service.IArticlesContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台内容信息 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Service
public class ArticlesContentServiceImpl extends ServiceImpl<ArticlesContentMapper, ArticlesContent> implements IArticlesContentService {

}
