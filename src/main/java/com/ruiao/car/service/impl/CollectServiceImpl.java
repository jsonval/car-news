package com.ruiao.car.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruiao.car.dto.ArticlesPraiseDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Collect;
import com.ruiao.car.entity.Praise;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.CollectMapper;
import com.ruiao.car.service.IArticlesService;
import com.ruiao.car.service.ICollectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruiao.car.utils.AccountHeaderUtil;
import com.ruiao.car.utils.AccountThreadLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 收藏 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@Service
public class CollectServiceImpl extends ServiceImpl<CollectMapper, Collect> implements ICollectService {

    @Autowired
    private IArticlesService articlesService;

    @Override
    public ResultDto getPage(Page page) {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        IPage<Collect> pageList = this.page(page, new QueryWrapper<Collect>().lambda().eq(Collect::getAccountId,accountThreadLocal.getAccount().getId()));
        pageList.getRecords().forEach(collect->{
            collect.setArticles(articlesService.getById(collect.getArticlesId()));
        });
        return new ResultDto(ResultEnum.SUCCESS, pageList);
    }

    @Override
    public ResultDto addCollect(ArticlesPraiseDto dto) {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        Collect collect = this.getOne(new QueryWrapper<Collect>().lambda()
                .eq(Collect::getAccountId,accountThreadLocal.getAccount().getId())
                .eq(Collect::getArticlesId,dto.getArticlesId())
                .eq(Collect::getCollectStatus,1));
        if(collect == null){
            collect = Collect.builder()
                    .appId(accountThreadLocal.getHeaderUtil().getAppId())
                    .accountId(accountThreadLocal.getAccount().getId())
                    .accountName(accountThreadLocal.getAccount().getNickName())
                    .accountAvatar(accountThreadLocal.getAccount().getAvatar())
                    .articlesId(dto.getArticlesId())
                    .build();
            this.save(collect);
            return ResultDto.get(ResultEnum.SUCCESS);
        }
        return new ResultDto(ResultEnum.SUCCESS.getErrCode(),"已收藏");
    }

    @Override
    public ResultDto deleteCollect(ArticlesPraiseDto dto) {
               AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        Collect collect = this.getOne(new QueryWrapper<Collect>().lambda()
                .eq(Collect::getAccountId,accountThreadLocal.getAccount().getId())
                .eq(Collect::getArticlesId,dto.getArticlesId())
                .eq(Collect::getCollectStatus,1));
        if(null!=collect){
            collect.setUpdateDate(new Date());
            collect.setCollectStatus(0);
            this.updateById(collect);
        }
        return ResultDto.get(ResultEnum.SUCCESS);
    }
}
