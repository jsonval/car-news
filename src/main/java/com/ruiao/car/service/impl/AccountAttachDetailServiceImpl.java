package com.ruiao.car.service.impl;

import com.ruiao.car.entity.AccountAttachDetail;
import com.ruiao.car.mapper.AccountAttachDetailMapper;
import com.ruiao.car.service.IAccountAttachDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户附加属性表 变更记录表 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-28
 */
@Service
public class AccountAttachDetailServiceImpl extends ServiceImpl<AccountAttachDetailMapper, AccountAttachDetail> implements IAccountAttachDetailService {

}
