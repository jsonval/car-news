package com.ruiao.car.service.impl;

import com.ruiao.car.entity.SignInWeekStatis;
import com.ruiao.car.mapper.SignInWeekStatisMapper;
import com.ruiao.car.service.ISignInWeekStatisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到周期统计 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-08-14
 */
@Service
public class SignInWeekStatisServiceImpl extends ServiceImpl<SignInWeekStatisMapper, SignInWeekStatis> implements ISignInWeekStatisService {

}
