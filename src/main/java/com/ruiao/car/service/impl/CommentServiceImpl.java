package com.ruiao.car.service.impl;

import com.ruiao.car.dto.AccountFollowDto;
import com.ruiao.car.dto.ArticlesCommentDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Comment;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.CommentMapper;
import com.ruiao.car.service.ICommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruiao.car.utils.AccountHeaderUtil;
import com.ruiao.car.utils.AccountThreadLocal;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements ICommentService {

    @Override
    public ResultDto addComment(ArticlesCommentDto dto) {
        AccountHeaderUtil accountThreadLocal =  AccountThreadLocal.get();
        Comment comment = Comment.builder()
                .appId(accountThreadLocal.getHeaderUtil().getAppId())
                .accountId(accountThreadLocal.getAccount().getId())
                .accountName(accountThreadLocal.getAccount().getNickName())
                .accountAvatar(accountThreadLocal.getAccount().getAvatar())
                .commentContent(dto.getCommentContent())
                .hostId(dto.getHostId())
                .hostType(dto.getHostType())
                .build();
        this.save(comment);
        return ResultDto.get(ResultEnum.SUCCESS);
    }

    @Override
    public ResultDto deleteComment(Long id) {
        Comment comment = this.getById(id);
        if(null!=comment){
            comment.setCommentStatus(0);
            comment.setUpdateDate(new Date());
            this.updateById(comment);
        }
        return ResultDto.get(ResultEnum.SUCCESS);
    }

}
