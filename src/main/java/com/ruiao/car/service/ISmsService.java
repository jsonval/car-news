package com.ruiao.car.service;

import com.ruiao.car.config.BusinessException;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Sms;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 短信发送记录 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface ISmsService extends IService<Sms> {

	void updateSms(Sms param);

	ResultDto sendSms(String smsType, String phone) throws BusinessException;

	Sms selectSmsByPhone(Sms param);

}
