package com.ruiao.car.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.ArticlesChannel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台内容频道 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface IArticlesChannelService extends IService<ArticlesChannel> {

    public ResultDto feedList(Page page, Long channelId);

}
