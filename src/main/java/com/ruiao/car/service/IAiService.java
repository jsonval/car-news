package com.ruiao.car.service;

import com.ruiao.car.dto.ResultDto;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author yinpingliu
 * @name IAiService
 * @time: 2020/7/3 下午3:04
 * @description: No Description
 */
public interface IAiService {

    String getAccessToken();

    ResultDto carModel(MultipartFile file);
    ResultDto carDamage(MultipartFile file);

}
