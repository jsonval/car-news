package com.ruiao.car.service;

import com.ruiao.car.entity.Articles;
import com.ruiao.car.entity.ArticlesOperate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 内容基础数据 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-17
 */
public interface IArticlesOperateService extends IService<ArticlesOperate> {

    void rebuildOp(Articles articles);

    void visit(Articles articles);

}
