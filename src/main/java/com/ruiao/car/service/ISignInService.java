package com.ruiao.car.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.SignIn;
import com.ruiao.car.enums.AccountAttachEnum;

/**
 * <p>
 * 签到 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-08-11
 */
public interface ISignInService extends IService<SignIn> {

    ResultDto addSignIn();

    Integer getUserSignInDayLevelNum();

    AccountAttachEnum getAttachByDay(Integer day);
}
