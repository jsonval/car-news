package com.ruiao.car.service.worm;

import com.ruiao.car.dto.WormDataDto;
import com.ruiao.car.enums.AppIdEnum;
import com.ruiao.car.utils.DateUtils;
import com.ruiao.car.utils.GsonUtils;

import cn.hutool.json.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ListUtils;

import java.util.*;

/**
 * Created by jsonval on 2020/6/12.
 */
@Slf4j
@Service
public class DcdAppWormService extends BaseWormService {

    /**
     * type = 2101(视频) 2002（文字）2312（文字）
     *
     * @return
     * @throws Exception
     */
    @Override
    List<WormDataDto> wormDate() throws Exception {
        List<WormDataDto> wormDataDtos = new ArrayList<>();

        String origin = "懂车帝";

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://www.dcdapp.com/motor/stream/api/news/feed/web/v47/?tt_from=load_more&sub_category=&category=motor_car&count=20&impression_info=%7B%22page_id%22%3A%22page_car_series%22%2C%22sub_tab%22%3A%22motor_car%22%2C%22product_name%22%3A%22web%22%7D&aid=1839&refer=1&channel=web&device_platform=web&web_id=794311376417720&motor_feed_extra_params=%7B%22new_feed%22%3A%20true%2C%20%22feed_type%22%3A%201%7D")
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        String rs = response.body().string();
        Map map = GsonUtils.get().fromJson(rs, HashMap.class);
        List<Map> datas = (List) map.get("data");
        for (Map data : datas) {
            try {
                Map info = (Map) data.get("info");
                System.out.println("=====info======" + GsonUtils.toJson(info));
                
                String title = (String) info.get("title");

                Double time = (Double) info.get("display_time");
                Date publishTime = new Date(time.longValue() * 1000);
                String abstract_content = (String) info.get("abstract_content");
                System.out.println("====abstract_content=====" + abstract_content);
                String sourceId = (String) data.get("unique_id_str");
                String author = (String) info.get("source");

//                String article_url = (String) info.get("article_url");

                List<String> images = new ArrayList<>();

                List<Map> image_list = (List<Map>) info.get("image_list");
                if (!ListUtils.isEmpty(image_list)) {
                    for (Map imageDetail : image_list) {
                        String url = (String) imageDetail.get("url");
                        images.add(url);
                    }
                }
                WormDataDto wormDataDto = WormDataDto.builder()
                        .appId(AppIdEnum.carNews)
                        .channelId("1")
                        .title(title)
                        .absContent(abstract_content)
                        .author(author)
                        .publishTime(publishTime)
                        .sourceId(sourceId)
                        .coverImages(images)
                        .contentUrl("https://m.dcdapp.com/motor/m/article/detail?group_id=" + sourceId)
                        .origin(origin).build();

                wormDataDtos.add(wormDataDto);
            } catch (Exception e) {
                log.error("懂车帝爬虫报错{}", e.getMessage());
            }

        }

        return wormDataDtos;
    }

    @Override
    String content(WormDataDto wormDataDto) throws Exception {

        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(wormDataDto.getContentUrl())
                    .get()
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = client.newCall(request).execute();
            Map map = GsonUtils.get().fromJson(response.body().string(), HashMap.class);
            Map data = (Map) map.get("data");
            String content = (String) data.get("content");
            System.out.println("===content=====" + content);
            return content;
        } catch (Exception e) {
            log.error("爬虫懂车帝详情报错{}", e.getMessage());
        }

        return null;

    }

    public static void main(String[] args) throws Exception {
//        OkHttpClient client = new OkHttpClient();
//
//        Request request = new Request.Builder()
//                .url("https://www.dcdapp.com/motor/stream/api/news/feed/web/v47/?tt_from=load_more&sub_category=&category=motor_car&count=20&max_behot_time=1591698427&impression_info=%7B%22page_id%22%3A%22page_car_series%22%2C%22sub_tab%22%3A%22motor_car%22%2C%22product_name%22%3A%22web%22%7D&aid=1839&refer=1&channel=web&device_platform=web&web_id=794311376417720&motor_feed_extra_params=%7B%22new_feed%22%3A%20true%2C%20%22feed_type%22%3A%201%7D")
//                .get()
//                .addHeader("cache-control", "no-cache")
//                .build();
//
//        Response response = client.newCall(request).execute();
//        String rs = response.body().string();
//        Map map = GsonUtils.get().fromJson(rs, HashMap.class);
//        List<Map> datas = (List) map.get("data");
//        for (Map data : datas) {
//            Map info = (Map) data.get("info");
//            String title = (String) info.get("title");
//
//            Double time = (Double) info.get("display_time");
//            Date publishTime = new Date(time.longValue() * 1000);
//            String abstract_content = (String) info.get("abstract_content");
//            String sourceId = (String) data.get("unique_id_str");
//            String origin = (String) info.get("source");
//            String article_url = (String) info.get("article_url");
//
//            List<String> images = new ArrayList<>();
//
//            List<Map> image_list = (List<Map>) data.get("image_list");
//            if (!ListUtils.isEmpty(image_list)) {
//                for (Map imageDetail : image_list) {
//                    String url = (String) imageDetail.get("url");
//                    images.add(url);
//                }
//            }
//            System.out.println(title);
//            System.out.println(article_url);
//            System.out.println(sourceId);
//
//
//
//        }


        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://m.dcdapp.com/motor/m/article/detail?group_id=6792083049312944654")
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        Map map = GsonUtils.get().fromJson(response.body().string(), HashMap.class);
        Map data = (Map) map.get("data");
        String content = (String) data.get("content");
        System.out.println(data);


    }


}
