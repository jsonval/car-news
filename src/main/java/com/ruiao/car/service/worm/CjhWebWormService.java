package com.ruiao.car.service.worm;

import com.ruiao.car.dto.CjhWebDto;
import com.ruiao.car.dto.WormDataDto;
import com.ruiao.car.enums.AppIdEnum;
import com.ruiao.car.utils.DateUtils;
import com.ruiao.car.utils.NetworkUtils;
import com.ruiao.car.utils.QiniuUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

/**
 * @author yinpingliu
 * @name QczjWebWormService
 * @time: 2020/6/12 下午3:16
 * @description: No Description
 */
@Slf4j
@Service
public class CjhWebWormService extends BaseWormService{

    public static final String CHE_JIA_HAO_BASE_URL = "https://chejiahao.autohome.com.cn";
    public static final List<CjhWebDto> cjhWebDtoList = Arrays.asList(
                CjhWebDto.builder().listUrl("https://chejiahao.autohome.com.cn/category/1#pvareaid=2808145").channelId("2").build(),
                CjhWebDto.builder().listUrl("https://chejiahao.autohome.com.cn/category/2#pvareaid=2808145").channelId("3").build(),
                CjhWebDto.builder().listUrl("https://chejiahao.autohome.com.cn/category/3#pvareaid=2808145").channelId("4").build(),
                CjhWebDto.builder().listUrl("https://chejiahao.autohome.com.cn/category/4#pvareaid=2808145").channelId("5").build(),
                CjhWebDto.builder().listUrl("https://chejiahao.autohome.com.cn/category/5#pvareaid=2808145").channelId("6").build(),
            CjhWebDto.builder().listUrl("https://chejiahao.autohome.com.cn/category/6#pvareaid=2808145").channelId("7").build());


    @Override
    List<WormDataDto> wormDate() {

        List<WormDataDto> wormDataDtoList = new ArrayList<>();

        CjhWebDto cjhWebDto = cjhWebDtoList.get(new Random().nextInt(5));

        Document doc = null;
        try {
            doc = Jsoup.connect(cjhWebDto.getListUrl())
                    .userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
                    .timeout(10000).get();
        } catch (IOException e) {
//            e.printStackTrace();
        }
        Elements rsVideoVElements = doc.getElementsByClass("author-vr identclass box videoV");
        Elements rsLightElements = doc.getElementsByClass("author-vr identclass box light");

        rsVideoVElements.forEach(element -> {
            Elements elements = element.getElementsByClass("videobtn");
            if(elements.size() == 0){
                WormDataDto wormDataDto = WormDataDto.builder()
                        .coverImages(handleImageList(element.getElementsByClass("videoA").get(0).getElementsByTag("img").eachAttr("data-original")))
                        .absContent(element.getElementsByClass("introduceTop text-overflow-all").get(0).text())
                        .contentUrl(CHE_JIA_HAO_BASE_URL+element.child(0).attr("href"))
                        .sourceId(element.attr("data-infoid"))
                        .appId(AppIdEnum.carNews)
                        .channelId(cjhWebDto.getChannelId())
                        .origin("汽车之家-车家号")
                        .build();
                wormDataDtoList.add(wormDataDto);
            }
        });

        rsLightElements.forEach(element -> {
            Elements elements = element.getElementsByClass("videobtn");
            if(elements.size() == 0){
                WormDataDto wormDataDto = WormDataDto.builder()
                        .coverImages(handleImageList(element.getElementsByClass("light-img").get(0).getElementsByClass("img").eachAttr("data-original")))
                        .absContent(element.getElementsByClass("introduceTop").get(0).text())
                        .contentUrl(CHE_JIA_HAO_BASE_URL+element.getElementsByClass("introduce fn-left").get(0).child(0).attr("href"))
                        .sourceId(element.attr("data-infoid"))
                        .appId(AppIdEnum.carNews)
                        .channelId(cjhWebDto.getChannelId())
                        .origin("汽车之家-车家号")
                        .build();
                wormDataDtoList.add(wormDataDto);
            }
        });

        return wormDataDtoList;

    }


    @Override
    String content(WormDataDto wormDataDto) throws Exception {
        Document doc = null;
        try {
            doc = Jsoup.connect(wormDataDto.getContentUrl())
                    .userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0")
                    .timeout(10000).get();
            Element contentElement;
            if(doc.getElementsByClass("video-container").size()>0){
                contentElement = doc.getElementsByClass("video-wrap").get(0);
                if(null!=contentElement){
                    handleImage(contentElement.getElementsByTag("img"));
                    wormDataDto.setContent(contentElement.html());
                }
            }else {
                contentElement = doc.getElementById("gallery-selector");
                if(null!=contentElement){
                    handleImage(contentElement.getElementsByTag("img"));
                    wormDataDto.setContent(contentElement.html());
                }
            }
            wormDataDto.setTitle(doc.getElementsByClass("title ").get(0).text());
            wormDataDto.setAuthor(doc.getElementsByClass("articleTag").get(0).child(0).text());
            wormDataDto.setPublishTime(DateUtils.parseDate(doc.getElementsByClass("articleTag").get(0).child(2).text(),"yyyy-MM-dd"));

        } catch (IOException e) {
//            e.printStackTrace();
        }
        return wormDataDto.getContent();
    }

    private List<String> handleImageList(List<String> list){
        if(list!=null){
            for (int i = 0; i < list.size(); i++) {
                list.set(i,"https:"+list.get(i).split("\\?")[0]);
            }
        }
        return list;
    }

    private Elements handleImage(Elements imageElements){
        imageElements.forEach(element -> {
            String imageUrl = QiniuUtils.fetchImage(element.attr("data-imageurl"));
            element.attr("src",imageUrl);
            element.attr("data-imageurl",imageUrl);
        });
        return imageElements;
    }


}
