package com.ruiao.car.service.worm;

/**
 * Created by jsonval on 2020/6/12.
 */
public interface IWormService {

    public void handleWormData() throws Exception;
}
