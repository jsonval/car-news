package com.ruiao.car.service;

import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Channel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台频道 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface IChannelService extends IService<Channel> {

    ResultDto channelList();
}
