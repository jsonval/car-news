package com.ruiao.car.service;

import com.ruiao.car.entity.ArticlesContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台内容信息 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface IArticlesContentService extends IService<ArticlesContent> {

}
