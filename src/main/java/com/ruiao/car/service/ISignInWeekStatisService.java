package com.ruiao.car.service;

import com.ruiao.car.entity.SignInWeekStatis;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 签到周期统计 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-08-14
 */
public interface ISignInWeekStatisService extends IService<SignInWeekStatis> {

}
