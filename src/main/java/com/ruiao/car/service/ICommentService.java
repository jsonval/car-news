package com.ruiao.car.service;

import com.ruiao.car.dto.AccountFollowDto;
import com.ruiao.car.dto.ArticlesCommentDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
public interface ICommentService extends IService<Comment> {

    ResultDto addComment(ArticlesCommentDto dto);
    ResultDto deleteComment(Long id);

}
