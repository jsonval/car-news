package com.ruiao.car.service;

import com.ruiao.car.entity.AccountAttachDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户附加属性表 变更记录表 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-28
 */
public interface IAccountAttachDetailService extends IService<AccountAttachDetail> {

}
