package com.ruiao.car.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruiao.car.dto.AccountFollowDto;
import com.ruiao.car.dto.ArticlesPraiseDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Collect;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收藏 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
public interface ICollectService extends IService<Collect> {

    ResultDto getPage(Page page);

    ResultDto addCollect(ArticlesPraiseDto dto);

    ResultDto deleteCollect(ArticlesPraiseDto dto);

}
