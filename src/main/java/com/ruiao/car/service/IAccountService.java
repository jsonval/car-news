package com.ruiao.car.service;

import com.ruiao.car.config.BusinessException;
import com.ruiao.car.dto.AccountDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.dto.UserDto;
import com.ruiao.car.dto.UserUpdateDto;
import com.ruiao.car.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface IAccountService extends IService<Account> {

	Account selectAccountByPhone(String phone);

	AccountDto selectAccountByToken(Long userId);

	ResultDto login(String phone, String password) throws BusinessException;

	ResultDto quickSmsLogin(String phone, String verificationCode) throws BusinessException;

	ResultDto reg(String phone, String password, String verificationCode) throws BusinessException;

	//ResultDto updateUser(UserDto dto) throws BusinessException;
	ResultDto updateUser(UserUpdateDto dto);

	ResultDto<AccountDto> getUserInfo();

}
