package com.ruiao.car.service;

import com.ruiao.car.dto.ArticlesPraiseDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Praise;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 点赞 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
public interface IPraiseService extends IService<Praise> {

    ResultDto addPraise(ArticlesPraiseDto dto);

    ResultDto deletePraise(ArticlesPraiseDto dto);

}
