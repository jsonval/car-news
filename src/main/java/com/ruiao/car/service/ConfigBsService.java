package com.ruiao.car.service;


import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.ConfigEntity;
import com.ruiao.car.enums.ConfigTypeEnum;
import com.ruiao.car.enums.RedisEnum;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.mapper.ConfigMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ConfigBsService {

    @Autowired
    private ConfigService configService;

    @Autowired
    private ConfigMapper configMapper;

    @Autowired
    private RedisService redisService;

    public ResultDto insertConfig(ConfigEntity configDto) {

        try {
            configDto.setCreateBy("sys");
            if (StringUtils.isEmpty(configDto.getIsValid())) {
                configDto.setIsValid("0");
            }
            if (configDto.getConfigType() == null) {
                configDto.setConfigType(ConfigTypeEnum.sys);
            }
            if (!StringUtils.isEmpty(configDto.getMaxVersion())) {
                configDto.setMaxVersion(configService.versionTransfer(configDto.getMaxVersion()));
            }
            if (!StringUtils.isEmpty(configDto.getMinVersion())) {
                configDto.setMinVersion(configService.versionTransfer(configDto.getMinVersion()));
            }

            String key = RedisEnum.CONFIG_CODE_VAL.getRedisKey(configDto.getParamCode());
            redisService.del(key);

            configMapper.insertConfig(configDto);
            return new ResultDto(ResultEnum.SUCCESS);
        } catch (Exception e) {
            log.error("insertConfig error :", e);
            return new ResultDto(ResultEnum.SYSTEM_ERROR);
        }

    }

    public ResultDto batchInsertUpdate(List<ConfigEntity> list) {

        try {
            for (ConfigEntity configDto : list) {
                Long id = configDto.getId();
                if (id == null) {
                    this.insertConfig(configDto);
                } else {
                    this.updateConfig(configDto);
                }
            }
            return new ResultDto(ResultEnum.SUCCESS);
        } catch (Exception e) {
            log.error("batchInsertUpdate error :", e);
            return new ResultDto(ResultEnum.SYSTEM_ERROR);
        }

    }


    /**
     * 获取配置列表
     *
     * @param paramCode
     * @return
     */
    public ResultDto<Page<ConfigEntity>> list(String appId, String paramCode, String paramDesc, String osType, String isValid,
                                              ConfigTypeEnum configType, Pageable pageable) {
        try {

            ConfigEntity configDto = new ConfigEntity();
            configDto.setParamCode(paramCode);
            configDto.setParamDesc(paramDesc);
            configDto.setOsType(osType);
            configDto.setIsValid(isValid);
            configDto.setAppId(appId);
            configDto.setConfigType(configType);

            Integer totalCount = configMapper.countConfigListBs(configDto);

            List<ConfigEntity> queryConfigListBs = configMapper.queryConfigListBs(configDto, pageable.getOffset(), pageable.getPageSize());
            for (ConfigEntity conf : queryConfigListBs) {
                if (!StringUtils.isEmpty(conf.getMaxVersion())) {
                    conf.setMaxVersion(configService.versionToSimple(conf.getMaxVersion()));
                }
                if (!StringUtils.isEmpty(conf.getMinVersion())) {
                    conf.setMinVersion(configService.versionToSimple(conf.getMinVersion()));
                }
            }
            return new ResultDto(ResultEnum.SUCCESS, new PageImpl(queryConfigListBs, pageable, totalCount));
        } catch (Exception e) {
            log.error("get bs list error :", e);
            return new ResultDto(ResultEnum.SYSTEM_ERROR);
        }
    }

    /**
     * 获取配置列表
     *
     * @param id
     * @return
     */
    public ResultDto<ConfigEntity> queryConfigById(Long id) {
        try {

            ConfigEntity result = configMapper.queryConfigById(id);
            if (result != null) {
                if (!StringUtils.isEmpty(result.getMaxVersion())) {
                    result.setMaxVersion(configService.versionToSimple(result.getMaxVersion()));
                }
                if (!StringUtils.isEmpty(result.getMinVersion())) {
                    result.setMinVersion(configService.versionToSimple(result.getMinVersion()));
                }
            }
            return new ResultDto(ResultEnum.SUCCESS, result);
        } catch (Exception e) {
            log.error("queryConfigByCode error :", e);
            return new ResultDto(ResultEnum.SYSTEM_ERROR);
        }
    }

    public ResultDto updateConfig(ConfigEntity configDto) {

        try {

            if (configDto.getConfigType() == null) {
                configDto.setConfigType(ConfigTypeEnum.sys);
            }
            if (!StringUtils.isEmpty(configDto.getMaxVersion())) {
                configDto.setMaxVersion(configService.versionTransfer(configDto.getMaxVersion()));
            }
            if (!StringUtils.isEmpty(configDto.getMinVersion())) {
                configDto.setMinVersion(configService.versionTransfer(configDto.getMinVersion()));
            }
            configMapper.updateConfig(configDto);

            Long id = configDto.getId();
            ConfigEntity config = configMapper.queryConfigById(id);
            String key = RedisEnum.CONFIG_CODE_VAL.getRedisKey(config.getParamCode());
            redisService.del(key);

            return new ResultDto(ResultEnum.SUCCESS);
        } catch (Exception e) {
            log.error("updateConfig error :", e);
            return new ResultDto(ResultEnum.SYSTEM_ERROR);
        }

    }

    public ResultDto updateConfigStatus(Long id, String isValid) {

        try {
            ConfigEntity configDto = new ConfigEntity();
            configDto.setId(id);
            configDto.setIsValid(isValid);
            configMapper.updateConfigStatus(configDto);

            ConfigEntity config = configMapper.queryConfigById(id);
            String key = RedisEnum.CONFIG_CODE_VAL.getRedisKey(config.getParamCode());
            redisService.del(key);

            return new ResultDto(ResultEnum.SUCCESS);
        } catch (Exception e) {
            log.error("updateConfigStatus error :", e);
            return new ResultDto(ResultEnum.SYSTEM_ERROR);
        }

    }


}
