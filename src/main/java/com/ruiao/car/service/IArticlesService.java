package com.ruiao.car.service;

import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Articles;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台内容 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface IArticlesService extends IService<Articles> {

    ResultDto detail(Long id);

    Articles getArticlesOperateInfo(Articles articles);

}
