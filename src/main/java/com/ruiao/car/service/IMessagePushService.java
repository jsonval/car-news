package com.ruiao.car.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruiao.car.dto.MessagePushDto;
import com.ruiao.car.dto.MessagePushQueryDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.MessagePush;

import java.util.List;

/**
 * <p>
 * 消息推送 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-07-13
 */
public interface IMessagePushService extends IService<MessagePush> {

    ResultDto<List<MessagePushDto>> listByLastUid(MessagePushQueryDto messagePushQueryDto);

    ResultDto<Boolean> readMessage(Long id);

    ResultDto<Integer> newMessageCount(MessagePushQueryDto messagePushQueryDto);

}
