package com.ruiao.car.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruiao.car.dto.AppVersionDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.AppVersion;

public interface IAppVersionService extends IService<AppVersion>{

	ResultDto<AppVersion> selectAppVersion(AppVersionDto appVersion);

}
