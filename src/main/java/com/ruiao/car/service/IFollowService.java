package com.ruiao.car.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruiao.car.dto.AccountFollowDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.Follow;

/**
 * <p>
 * 关注 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
public interface IFollowService extends IService<Follow> {

    ResultDto addFollow(AccountFollowDto dto);

    ResultDto deleteFollow(AccountFollowDto dto);
}
