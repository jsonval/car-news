package com.ruiao.car.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruiao.car.dto.ArticlesIntegralDto;
import com.ruiao.car.dto.ArticlesIntegralGiveDto;
import com.ruiao.car.dto.ArticlesIntegralTotalDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.AccountAttach;
import com.ruiao.car.entity.AccountAttachDetail;

/**
 * <p>
 * 用户附加属性表，存储积分等等信息 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-28
 */
public interface IAccountAttachService extends IService<AccountAttach> {

    ResultDto addArticlesIntegral(ArticlesIntegralDto articlesIntegralDto);

    ResultDto addArticlesIntegralFromIncentiveVideo();

    void saveArticlesIntegral(AccountAttachDetail accountAttachDetail);

    ResultDto<ArticlesIntegralTotalDto> getPage(Page page);

    ResultDto giveArticlesIntegral(ArticlesIntegralGiveDto articlesIntegralGiveDto);

}
