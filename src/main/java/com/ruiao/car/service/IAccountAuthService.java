package com.ruiao.car.service;

import com.ruiao.car.entity.AccountAuth;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户登录auth 服务类
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface IAccountAuthService extends IService<AccountAuth> {

}
