package com.ruiao.car;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;

/**
 *
 */
@SpringBootApplication
@EnableTransactionManagement
public class Application {

    @PostConstruct
    void started() {

    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
