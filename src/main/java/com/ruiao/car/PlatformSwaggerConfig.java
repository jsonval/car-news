/**
 * @since: 2015年3月23日
 * @copyright: copyright (c) 2015,www.linkin.mobi All Rights Reserved.
 */
package com.ruiao.car;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class PlatformSwaggerConfig {

    @Value("${swagger.host.url}")
    private String swaggerHost;

    @Bean
    public Docket buildDocket() {


        ParameterBuilder ticketPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        ticketPar.name("auth").description("登录auth")//name表示名称，description表示描述
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).defaultValue("").build();//required表示是否必填，defaultvalue表示默认值
        pars.add(ticketPar.build());//添加完此处一定要把下边的带***的也加上否则不生效


        ticketPar.name("token").description("登录token")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).defaultValue("").build();
        pars.add(ticketPar.build());

        ticketPar.name("osType").description("系统类型")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(true).defaultValue("server").build();
        pars.add(ticketPar.build());


        ticketPar.name("appId").description("appId")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(true).defaultValue("carNews").build();
        pars.add(ticketPar.build());

        ticketPar.name("appVersion").description("appVersion")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(true).defaultValue("1.0.1(50)").build();
        pars.add(ticketPar.build());

        ticketPar.name("deviceId").description("设备ID")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).defaultValue("").build();
        pars.add(ticketPar.build());


        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(buildApiInf())
                .host(swaggerHost)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.ruiao"))//controller路径
                .paths(PathSelectors.any()).build().globalOperationParameters(pars);

    }

    protected Contact getContact() {

        return new Contact("jsonval", "", "jsonval@163.com");
    }

    private ApiInfo buildApiInf() {
        return new ApiInfoBuilder()
                .title("平台 Docs")
                .termsOfServiceUrl("http://www.github.com/kongchen/swagger-maven-plugin")
                .build();

    }
}

