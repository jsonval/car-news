package com.ruiao.car.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 内容基础数据 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-17
 */
@RestController
@RequestMapping("/car/articles-operate")
public class ArticlesOperateController {

}
