package com.ruiao.car.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户附加属性表 变更记录表 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-28
 */
@RestController
@RequestMapping("/car/account-attach-detail")
public class AccountAttachDetailController {

}
