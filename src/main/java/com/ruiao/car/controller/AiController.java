package com.ruiao.car.controller;

import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.service.IAiService;
import com.ruiao.car.utils.annotation.RequiresPermissions;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author yinpingliu
 * @name AiController
 * @time: 2020/7/3 下午3:03
 * @description: No Description
 */
@RestController
@RequestMapping("/ai/distinguish")
@Api(value = "AiController",description = "ai")
public class AiController {

    @Resource
    private IAiService aiService;

//    @GetMapping(value = "/getAccessToken")
//    public String getAccessToken(){
//        return aiService.getAccessToken();
//    }


    @PostMapping(value = "/carModel")
    @ApiOperation(value = "识别车型")
    @RequiresPermissions
    public ResultDto carModel(@RequestParam(value = "file") MultipartFile file) {
        return new ResultDto(ResultEnum.SUCCESS,aiService.carModel(file));
    }

    @PostMapping(value = "/carDamage")
    @ApiOperation(value = "车辆外观损伤识别")
    @RequiresPermissions
    public ResultDto carDamage(@RequestParam(value = "file") MultipartFile file) {
        return new ResultDto(ResultEnum.SUCCESS,aiService.carDamage(file));
    }

}
