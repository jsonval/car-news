package com.ruiao.car.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruiao.car.dto.AccountDto;
import com.ruiao.car.dto.AppVersionDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.AppVersion;
import com.ruiao.car.service.IAppVersionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(value = "AppVersionController",description = "版本更新")
public class AppVersionController {

	
	@Autowired
	private IAppVersionService appVersionService;
	
	@PostMapping(value = "/appVersion/selectAppVersion")
    @ApiOperation(value = "查询最新的App版本号", notes = "查询最新的App版本号")
    public ResultDto<AppVersion> selectAppVersion(HttpServletRequest request,@RequestBody @ApiParam AppVersionDto appVersion){
		return appVersionService.selectAppVersion(appVersion);
	}
}
