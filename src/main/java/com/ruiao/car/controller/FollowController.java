package com.ruiao.car.controller;
import com.ruiao.car.config.BusinessException;
import com.ruiao.car.dto.AccountFollowDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.service.IFollowService;
import com.ruiao.car.utils.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
/**
 * <p>
 * 关注 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@RestController
@RequestMapping(value = "follow")
@Api(value = "followController",description = "关注")
public class FollowController {

    @Autowired
    private IFollowService followService;

    @PostMapping(value = "/add")
    @ApiOperation(value = "关注")
    @RequiresPermissions
    public ResultDto addFollow(@RequestBody AccountFollowDto followDto) throws Exception{
        if(null == followDto.getAccountId() && StringUtils.isEmpty(followDto.getNickName())){
            throw new BusinessException(ResultEnum.LOSE_PARAM);
        }
        return followService.addFollow(followDto);
    }

    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "取消关注")
    @RequiresPermissions
    public ResultDto deleteFollow(@RequestBody AccountFollowDto followDto){
        return followService.deleteFollow(followDto);
    }

}
