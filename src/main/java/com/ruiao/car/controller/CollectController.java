package com.ruiao.car.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruiao.car.dto.AccountFollowDto;
import com.ruiao.car.dto.ArticlesPraiseDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.service.ICollectService;
import com.ruiao.car.utils.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 收藏 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@RestController
@RequestMapping(value = "collect")
@Api(value = "collectController",description = "收藏")
public class CollectController {

    @Autowired
    private ICollectService collectService;

    @PostMapping(value = "/list")
    @ApiOperation(value = "收藏列表")
    @RequiresPermissions
    public ResultDto list(Page page){
        return collectService.getPage(page);
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "收藏")
    @RequiresPermissions
    public ResultDto addCollect(@RequestBody @Valid ArticlesPraiseDto articlesPraise){
        return collectService.addCollect(articlesPraise);
    }

    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "取消收藏")
    @RequiresPermissions
    public ResultDto deleteCollect(@RequestBody @Valid ArticlesPraiseDto articlesPraise) {
        return collectService.deleteCollect(articlesPraise);
    }

}
