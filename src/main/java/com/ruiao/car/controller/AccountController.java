package com.ruiao.car.controller;
import com.ruiao.car.dto.AccountDto;
import com.ruiao.car.dto.UserUpdateDto;
import com.ruiao.car.utils.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruiao.car.config.BusinessException;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.dto.UserDto;
import com.ruiao.car.service.IAccountService;
import com.ruiao.car.utils.annotation.RequiresPermissions;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@RestController
@RequestMapping("/car/account")
public class AccountController {
	
	@Autowired
	private IAccountService accountService;
	
	@SuppressWarnings({ "rawtypes", "null" })
	@ApiOperation(value = "注册", notes = "用户注册")
	@PostMapping(value = "/reg")
	public ResultDto reg( @RequestBody @ApiParam UserDto req) throws BusinessException {
		return accountService.reg(req.getPhone(),req.getPassword(),req.getVerificationCode());
	}
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "手机验证码登录", notes = "手机验证码登录")
	@PostMapping(value = "/quickSmsLogin")
	public ResultDto quickSmsLogin(
			 @RequestBody @ApiParam UserDto req) throws BusinessException {
		return accountService.quickSmsLogin(req.getPhone(),req.getVerificationCode());
	}
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "手机号密码登录", notes = "手机号密码登录")
	@PostMapping(value = "/login")
	public ResultDto login(
			 @RequestBody @ApiParam UserDto req) throws BusinessException{
		return accountService.login(req.getPhone(),req.getPassword());
	}
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "修改个人信息", notes = "修改个人信息")
	@PostMapping(value = "/updateUser")
	@RequiresPermissions
	public ResultDto updateUser(@RequestBody UserUpdateDto dto) throws BusinessException{
		return accountService.updateUser(dto);
	}


	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "个人中心-获取用户信息", notes = "个人中心-获取用户信息")
	@GetMapping(value = "/getUserInfo")
	@RequiresPermissions
	public ResultDto<AccountDto> getUserInfo() throws BusinessException{
		return accountService.getUserInfo();
	}
	
	
	

}
