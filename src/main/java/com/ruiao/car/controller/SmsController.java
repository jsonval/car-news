package com.ruiao.car.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruiao.car.config.BusinessException;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.dto.SmsDto;
import com.ruiao.car.service.ISmsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 短信发送记录 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@RestController
@RequestMapping("/car/sms")
public class SmsController {
	
	@Autowired
	private ISmsService smsService;

	@ApiOperation(value = "短信发送", notes = "smsType:1注册，2登录，3修改密码，4其他 ")
	@PostMapping(value = "/sendSms")
	public ResultDto sendSms( @RequestBody @ApiParam SmsDto req) throws BusinessException {
		return smsService.sendSms(req.getSmsType(),req.getPhone());
	}
}
