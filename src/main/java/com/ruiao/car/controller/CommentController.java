package com.ruiao.car.controller;
import com.ruiao.car.dto.ArticlesCommentDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.service.ICommentService;
import com.ruiao.car.utils.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 评论 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@RestController
@RequestMapping(value = "comment")
@Api(value = "commentController",description = "评论")
public class CommentController {

    @Autowired
    private ICommentService commentService;

    @PostMapping(value = "/add")
    @ApiOperation(value = "评论")
    @RequiresPermissions
    public ResultDto addCollect(@RequestBody @Valid ArticlesCommentDto commentDto){
        return commentService.addComment(commentDto);
    }

    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(value = "取消评论")
    @RequiresPermissions
    public ResultDto deleteCollect(@PathVariable Long id) {
        return commentService.deleteComment(id);
    }

}
