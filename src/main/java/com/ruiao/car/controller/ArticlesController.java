package com.ruiao.car.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.service.IArticlesChannelService;
import com.ruiao.car.service.IArticlesService;
import com.ruiao.car.service.IChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 平台内容 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@RestController
@RequestMapping("/car/articles")
public class ArticlesController {

    @Autowired
    private IArticlesChannelService articlesChannelService;

    @Autowired
    private IArticlesService articlesService;

    @Autowired
    private IChannelService channelService;


    @ApiOperation(value = "渠道列表")
    @GetMapping("channelList")
    public ResultDto channelList() {
        return channelService.channelList();
    }

    @ApiOperation(value = "传入渠道id，获取对应渠道的资讯列表")
    @GetMapping("feedList/{channelId}")
    public ResultDto feedList(Page page, @PathVariable Long channelId) {
        return articlesChannelService.feedList(page, channelId);
    }

    @ApiOperation(value = "资讯详情")
    @GetMapping("detail/{id}")
    public ResultDto detail(@PathVariable Long id) {
        return articlesService.detail(id);
    }


}
