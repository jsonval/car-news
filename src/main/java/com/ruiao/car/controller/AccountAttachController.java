package com.ruiao.car.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruiao.car.dto.ArticlesIntegralDto;
import com.ruiao.car.dto.ArticlesIntegralGiveDto;
import com.ruiao.car.dto.ArticlesIntegralTotalDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.service.IAccountAttachService;
import com.ruiao.car.utils.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 用户附加属性表，存储积分等等信息 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-28
 */
@RestController
@RequestMapping("/account/attach")
@Api(value = "AccountAttachController",description = "积分")
public class AccountAttachController {

    @Resource
    private IAccountAttachService accountAttachService;

    @PostMapping(value = "/integral/add")
    @ApiOperation(value = "增加积分")
    @RequiresPermissions
    public ResultDto addArticlesIntegral(@RequestBody @Valid ArticlesIntegralDto articlesIntegralDto){
        return accountAttachService.addArticlesIntegral(articlesIntegralDto);
    }

    @PostMapping(value = "/integral/incentive/video/add")
    @ApiOperation(value = "增加积分-观看激励视频")
    @RequiresPermissions
    public ResultDto addIncentiveVideoIntegral(){
        return accountAttachService.addArticlesIntegralFromIncentiveVideo();
    }

    @GetMapping(value = "/integral/list")
    @ApiOperation(value = "积分列表")
    @RequiresPermissions
    public ResultDto<ArticlesIntegralTotalDto> articlesIntegralList(Page page){
        return accountAttachService.getPage(page);
    }

    @PostMapping(value = "/integral/give")
    @ApiOperation(value = "转赠积分")
    @RequiresPermissions
    public ResultDto giveArticlesIntegral(@RequestBody @Valid ArticlesIntegralGiveDto articlesIntegralGiveDto){
        return accountAttachService.giveArticlesIntegral(articlesIntegralGiveDto);
    }


}
