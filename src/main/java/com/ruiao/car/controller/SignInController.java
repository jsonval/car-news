package com.ruiao.car.controller;


import com.ruiao.car.dto.ArticlesPraiseDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.dto.SignInListDto;
import com.ruiao.car.enums.AccountAttachEnum;
import com.ruiao.car.enums.IResultEnum;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.service.ISignInService;
import com.ruiao.car.utils.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 签到 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-08-11
 */
@RestController
@RequestMapping(value = "signIn")
@Api(value = "signInController",description = "签到")
public class SignInController {

    @Resource
    private ISignInService signInService;

    @PostMapping(value = "/add")
    @ApiOperation(value = "今日签到")
    @RequiresPermissions
    public ResultDto addSignIn(){
        return signInService.addSignIn();
    }

    @GetMapping(value = "/list")
    @ApiOperation(value = "签到列表")
    @RequiresPermissions
    public ResultDto<List<SignInListDto>> list(){
        List<SignInListDto> signInListDtoList = new ArrayList<>(7);

        signInListDtoList.add(SignInListDto.builder().day(1).attachValue(AccountAttachEnum.SIGN_IN_DAY_1.getAttachValue()).isObtain(false).build());
        signInListDtoList.add(SignInListDto.builder().day(2).attachValue(AccountAttachEnum.SIGN_IN_DAY_2.getAttachValue()).isObtain(false).build());
        signInListDtoList.add(SignInListDto.builder().day(3).attachValue(AccountAttachEnum.SIGN_IN_DAY_3.getAttachValue()).isObtain(false).build());
        signInListDtoList.add(SignInListDto.builder().day(4).attachValue(AccountAttachEnum.SIGN_IN_DAY_4.getAttachValue()).isObtain(false).build());
        signInListDtoList.add(SignInListDto.builder().day(5).attachValue(AccountAttachEnum.SIGN_IN_DAY_5.getAttachValue()).isObtain(false).build());
        signInListDtoList.add(SignInListDto.builder().day(6).attachValue(AccountAttachEnum.SIGN_IN_DAY_6.getAttachValue()).isObtain(false).build());
        signInListDtoList.add(SignInListDto.builder().day(7).attachValue(AccountAttachEnum.SIGN_IN_DAY_7.getAttachValue()).isObtain(false).build());

        int day = signInService.getUserSignInDayLevelNum();

        signInListDtoList.forEach(signInListDto -> {
            if(signInListDto.getDay() == day){
                signInListDto.setIsObtain(true);
            }
        });

        return new ResultDto(ResultEnum.SUCCESS,signInListDtoList);
    }

}
