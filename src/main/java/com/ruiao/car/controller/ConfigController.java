package com.ruiao.car.controller;


import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.service.ConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@Slf4j
@Controller
@Api(tags = "C端云控中心", description = "ConfigController")
@RequestMapping("/common/config/client")
public class ConfigController {

    @Autowired
    private ConfigService configService;


    /**
     *根据paramCode获取配置
     * @param paramCodes
     * @return
     */
    @ApiOperation(value = "跨应用统一云控配置查询(支持传入多个code、可查询系统云控、广告条、任务栏、功能墙等配置信息)",notes = "查询参数、匹配值可动态匹配")
    @RequestMapping(value = "/getConfigs/V2",method = RequestMethod.GET)
    @ResponseBody
    public ResultDto<Map<String,Object>> getConfigsV2(@RequestParam("paramCodes")String paramCodes,
                                                      @RequestParam(value="configVersion",required =false,defaultValue = "0")String configVersion,
                                                      @RequestParam(value = "appId") String appId,
                                                      @RequestParam(value = "osType",required =false) String osType,
                                                      @RequestParam(value = "appVersion",required =false) String appVersion,
                                                      @RequestHeader(value = "appChannel",required =false) String appChannel) {
      /*  log.info(String.format("getConfigs,paramCodes:%s,configVersion:%s,appId:%s," +
                "osType:%s,appVersion:%s",paramCodes,configVersion,appId,osType,appVersion));*/

        if(StringUtils.isBlank(paramCodes)){
            return new ResultDto(ResultEnum.LOSE_PARAM);
        }
        ResultDto<Map<String,Object>> resultEntity = configService.parseHitConfigV2( paramCodes,configVersion,appId, osType, appVersion);
        return resultEntity;
    }


}
