package com.ruiao.car.controller;

import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.enums.ResultEnum;
import com.ruiao.car.utils.ImageUtil;
import com.ruiao.car.utils.QiniuUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author yinpingliu
 * @name UploadController
 * @time: 2020/6/16 下午5:40
 * @description: No Description
 */
@RestController
@RequestMapping(value = "upload")
@Api(value = "uploadController",description = "上传")
public class UploadController {


    @PostMapping(value = "/image")
    @ResponseBody
    @ApiOperation(value = "上传图片")
    public ResultDto uploadImage(@RequestParam(value = "file") MultipartFile file) {
        return new ResultDto(ResultEnum.SUCCESS,QiniuUtils.upload(file,ImageUtil.generateImageName(file)));
    }

}
