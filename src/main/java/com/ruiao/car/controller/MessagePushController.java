package com.ruiao.car.controller;


import com.ruiao.car.dto.MessagePushDto;
import com.ruiao.car.dto.MessagePushQueryDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.service.IMessagePushService;
import com.ruiao.car.utils.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 消息推送 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-07-13
 */
@RestController
@RequestMapping("/message/push")
@Api(value = "MessagePushController",description = "消息")
public class MessagePushController {

    @Resource
    private IMessagePushService messagePushService;

    @GetMapping(value = "/list")
    @ApiOperation(value = "消息列表")
    public ResultDto<List<MessagePushDto>> listByLastUid(MessagePushQueryDto messagePushQueryDto){
        return messagePushService.listByLastUid(messagePushQueryDto);
    }

    @PostMapping(value = "/read/{id}")
    @ApiOperation(value = "消息已读")
    @RequiresPermissions
    public ResultDto<Boolean> readMessage(@PathVariable Long id){
        return messagePushService.readMessage(id);
    }

    @GetMapping(value = "/count")
    @ApiOperation(value = "未读消息数量")
    public ResultDto<Integer> newMessageCount(MessagePushQueryDto messagePushQueryDto){
        return messagePushService.newMessageCount(messagePushQueryDto);
    }

}
