package com.ruiao.car.controller;



import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.entity.ConfigEntity;
import com.ruiao.car.enums.ConfigTypeEnum;
import com.ruiao.car.service.ConfigBsService;
import com.ruiao.car.utils.GsonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@Api(tags = "OP端云控中心", description = "OP端云控中心")
@RequestMapping("/common/config/op")
public class ConfigOpController {

    @Autowired
    private ConfigBsService configBsService;

    @ApiOperation(value = "新增云控配置")
    @RequestMapping(value="/insertConfig",method = RequestMethod.POST)
    @ResponseBody
    public ResultDto insertConfig(@RequestBody ConfigEntity params) {

        log.info(String.format("/bs/insertConfig,params: %s",new Gson().toJson(params)));

        ResultDto resultEntity = configBsService.insertConfig(params);
        return resultEntity;
    }

    @ApiOperation(value = "批量新增/修改云控配置")
    @RequestMapping(value="/batchInsertUpdate",method = RequestMethod.POST)
    @ResponseBody
    public ResultDto batchInsertUpdate(@RequestBody String params) {
        log.info(String.format("/bs/batchInsertUpdate,params: %s",params));
        List<ConfigEntity> list = GsonUtils.get().fromJson(params,new TypeToken<List<ConfigEntity>>(){}.getType());
        ResultDto resultEntity = configBsService.batchInsertUpdate(list);
        return resultEntity;
    }

    @ApiOperation(value = "云控列表")
    @RequestMapping(value="/list",method = RequestMethod.GET)
    @ResponseBody
    public ResultDto list(@RequestParam(value="paramCode",required =false) String paramCode,
                          @RequestParam(value="paramDesc",required =false) String paramDesc,
                             @RequestParam(value="configType",defaultValue = "sys") ConfigTypeEnum configType,
                             @RequestParam(value="osType",required =false) String osType,
                             @RequestParam(value="isValid",required =false) String isValid,
                             @RequestParam(value="appId",required =false) String appId,
                             @PageableDefault() final Pageable pageable) {

        ResultDto<Page<ConfigEntity>> resultEntity = null;

        resultEntity = configBsService.list(appId,paramCode,paramDesc,osType,isValid,configType,pageable);

        return resultEntity;
    }

    @ApiOperation(value = "查询云控配置详情")
    @RequestMapping(value="/queryParamById",method = RequestMethod.GET)
    @ResponseBody
    public ResultDto queryParamByCode(@RequestParam(value="id") Long id) {

        ResultDto resultEntity = configBsService.queryConfigById(id);

        return resultEntity;
    }


    @ApiOperation(value = "启用禁用配置项")
    @RequestMapping(value="/updateStatus",method = RequestMethod.POST)
    @ResponseBody
    public ResultDto updateStatus(@RequestParam(value="id")Long id, @RequestParam(value="isValid")String isValid) {

        ResultDto resultEntity = configBsService.updateConfigStatus(id,isValid);

        return resultEntity;
    }

    @ApiOperation(value = "修改配置")
    @RequestMapping(value="/updateConfig/{id}",method = RequestMethod.POST)
    @ResponseBody
    public ResultDto updateConfig(@PathVariable Long id, @RequestBody ConfigEntity params) {

        params.setId(id);
        ResultDto resultEntity = configBsService.updateConfig(params);
        return resultEntity;
    }





}
