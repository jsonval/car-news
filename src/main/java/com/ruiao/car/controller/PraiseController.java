package com.ruiao.car.controller;
import com.ruiao.car.dto.ArticlesPraiseDto;
import com.ruiao.car.dto.ResultDto;
import com.ruiao.car.service.IPraiseService;
import com.ruiao.car.utils.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 点赞 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@RestController
@RequestMapping(value = "praise")
@Api(value = "praiseController",description = "点赞")
public class PraiseController {

    @Resource
    private IPraiseService praiseService;

    @PostMapping(value = "/add")
    @ApiOperation(value = "点赞")
    @RequiresPermissions
    public ResultDto addPraise(@RequestBody @Valid ArticlesPraiseDto articlesPraise){
        return praiseService.addPraise(articlesPraise);
    }

    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "取消点赞")
    @RequiresPermissions
    public ResultDto deletePraise(@RequestBody ArticlesPraiseDto articlesPraise){
        return praiseService.deletePraise(articlesPraise);
    }


}
