package com.ruiao.car.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户登录auth 前端控制器
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@RestController
@RequestMapping("/car/account-auth")
public class AccountAuthController {

}
