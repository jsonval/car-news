package com.ruiao.car.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.*;


/**
 * Created by jsonval on 2019/12/7.
 */
@Configuration
public class RestConfig {
    @Bean
    public RestTemplate restTemplate() {
        List<MediaType> supportedMediaTypes = new ArrayList<>(3);
        supportedMediaTypes.add(TEXT_PLAIN);
        supportedMediaTypes.add(APPLICATION_JSON);
        supportedMediaTypes.add(APPLICATION_JSON_UTF8);

        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setSupportedMediaTypes(supportedMediaTypes);

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>(1);
        messageConverters.add(messageConverter);

        return new RestTemplate(messageConverters);
    }

}
