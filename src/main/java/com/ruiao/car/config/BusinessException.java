package com.ruiao.car.config;


import com.ruiao.car.enums.IResultEnum;

/**
 * Created by jsonval on 2019/7/8.
 */
public class BusinessException extends Exception {

    IResultEnum resultEnum;

    public BusinessException(IResultEnum resultEnum) {
        super(resultEnum.getErrMsg());
        this.resultEnum = resultEnum;
    }

    public IResultEnum getResultEnum() {
        return resultEnum;
    }

    public void setResultEnum(IResultEnum resultEnum) {
        this.resultEnum = resultEnum;
    }
}
