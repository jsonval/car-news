package com.ruiao.car.mapper;

import com.ruiao.car.entity.Sms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 短信发送记录 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface SmsMapper extends BaseMapper<Sms> {

}
