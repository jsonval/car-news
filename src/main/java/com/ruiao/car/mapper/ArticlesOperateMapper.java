package com.ruiao.car.mapper;

import com.ruiao.car.entity.ArticlesOperate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 内容基础数据 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-17
 */
public interface ArticlesOperateMapper extends BaseMapper<ArticlesOperate> {

}
