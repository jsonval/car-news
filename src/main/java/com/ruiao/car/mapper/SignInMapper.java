package com.ruiao.car.mapper;

import com.ruiao.car.entity.SignIn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 签到 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-08-11
 */
public interface SignInMapper extends BaseMapper<SignIn> {

}
