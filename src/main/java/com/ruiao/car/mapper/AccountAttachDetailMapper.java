package com.ruiao.car.mapper;

import com.ruiao.car.entity.AccountAttachDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户附加属性表 变更记录表 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-28
 */
public interface AccountAttachDetailMapper extends BaseMapper<AccountAttachDetail> {

}
