package com.ruiao.car.mapper;

import com.ruiao.car.entity.Collect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 收藏 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
public interface CollectMapper extends BaseMapper<Collect> {

}
