package com.ruiao.car.mapper;

import com.ruiao.car.entity.AccountAuth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户登录auth Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface AccountAuthMapper extends BaseMapper<AccountAuth> {

}
