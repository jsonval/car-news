package com.ruiao.car.mapper;

import com.ruiao.car.entity.SignInWeekStatis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 签到周期统计 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-08-14
 */
public interface SignInWeekStatisMapper extends BaseMapper<SignInWeekStatis> {

}
