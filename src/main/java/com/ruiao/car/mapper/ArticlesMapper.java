package com.ruiao.car.mapper;

import com.ruiao.car.entity.Articles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 平台内容 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface ArticlesMapper extends BaseMapper<Articles> {

}
