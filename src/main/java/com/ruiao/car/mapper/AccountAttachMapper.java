package com.ruiao.car.mapper;

import com.ruiao.car.entity.AccountAttach;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户附加属性表，存储积分等等信息 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-28
 */
public interface AccountAttachMapper extends BaseMapper<AccountAttach> {

}
