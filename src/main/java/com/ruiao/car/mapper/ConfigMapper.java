package com.ruiao.car.mapper;

import com.ruiao.car.entity.ConfigEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ConfigMapper {

    int insertConfig(@Param("config") ConfigEntity param);

    List<ConfigEntity> queryConfigByCondition(@Param("param") ConfigEntity param);

    ConfigEntity queryConfigById(@Param("id") Long id);


    Integer countConfigListBs(@Param("param") ConfigEntity param);

    List<ConfigEntity> queryConfigListBs(@Param("param") ConfigEntity param, @Param("pageIndex") Long pageIndex,
                                         @Param("pageSize") Integer pageSize);


    int updateConfig(@Param("param") ConfigEntity param);

    int updateConfigStatus(@Param("param") ConfigEntity param);

}
