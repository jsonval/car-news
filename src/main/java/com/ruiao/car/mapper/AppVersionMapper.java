package com.ruiao.car.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruiao.car.entity.AppVersion;

public interface AppVersionMapper extends BaseMapper<AppVersion> {

}
