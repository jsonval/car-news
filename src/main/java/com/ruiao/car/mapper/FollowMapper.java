package com.ruiao.car.mapper;

import com.ruiao.car.entity.Follow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 关注 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
public interface FollowMapper extends BaseMapper<Follow> {

}
