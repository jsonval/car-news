package com.ruiao.car.mapper;

import com.ruiao.car.entity.MessagePush;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消息推送 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-07-13
 */
public interface MessagePushMapper extends BaseMapper<MessagePush> {

}
