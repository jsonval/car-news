package com.ruiao.car.mapper;

import com.ruiao.car.entity.Praise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 点赞 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
public interface PraiseMapper extends BaseMapper<Praise> {

}
