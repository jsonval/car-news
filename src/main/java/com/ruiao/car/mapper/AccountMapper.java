package com.ruiao.car.mapper;

import com.ruiao.car.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
public interface AccountMapper extends BaseMapper<Account> {

}
