package com.ruiao.car.entity;

import java.time.LocalDateTime;
import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 短信发送记录
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Sms implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Long id;
    
    private String appId;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 操作用户id
     */
    private Long accountId;

    /**
     * ip地址
     */
    private String ipAddress;

    /**
     * 验证码
     */
    private String code;

    /**
     * 发送渠道
     */
    private String channel;

    /**
     * 0删除，1成功
     */
    private Byte status;

    /**
     * 添加时间
     */
    private Date createDate;

    /**
     * 短信接口调用返回
     */
    private String remark;

    /**
     * 1注册，2登录，3修改密码，4其他
     */
    private Byte type;

}
