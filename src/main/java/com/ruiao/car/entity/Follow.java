package com.ruiao.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 关注
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
public class Follow implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * appid
     */
    private String appId;

    /**
     * 被关注用户id
     */
    private Long followAccountId;

    /**
     * 操作用户id
     */
    private Long accountId;

    /**
     * 操作用户名称
     */
    private String accountName;
    private String accountAvatar;

    /**
     * 0删除，1成功
     */
    private Integer followStatus;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 更新日期
     */
    private Date updateDate;

    @Tolerate
    public Follow() {
    }
}
