package com.ruiao.car.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 平台内容信息
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ArticlesContent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 内容自增id
     */
    private String articlesId;

    private String content;

    /**
     * 状态 0：不可用  1：可用
     */
    private boolean status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 版本字段
     */
    private Integer version;


    @Tolerate
    public ArticlesContent() {

    }

}
