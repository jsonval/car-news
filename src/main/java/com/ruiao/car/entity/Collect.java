package com.ruiao.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 收藏
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
public class Collect implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * appid
     */
    private String appId;

    /**
     * 内容自增id
     */
    private String articlesId;

    /**
     * 操作用户id
     */
    private Long accountId;

    /**
     * 操作用户名称
     */
    private String accountName;
    private String accountAvatar;

    /**
     * ip地址
     */
    private String ipAddress;

    /**
     * 0删除，1成功
     */
    private Integer collectStatus;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 更新日期
     */
    private Date updateDate;

    @TableField(exist = false)
    private Articles articles;

    @Tolerate
    public Collect() {
    }
}
