package com.ruiao.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 消息推送
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-07-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
public class MessagePush implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long accountId;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 数据
     */
    private String data;

    /**
     * 0-未读，1-已读
     */
    private Integer receipt;

    /**
     * 是否删除，1正常，0删除
     */
    private Integer messageStatus;

    /**
     * 预定推送时间
     */
    private Date scheduleDate;

    /**
     * 实际推送时间
     */
    private Date pushDate;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 更新日期
     */
    private Date updateDate;

    @Tolerate
    public MessagePush() {
    }
}
