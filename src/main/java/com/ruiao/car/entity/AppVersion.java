package com.ruiao.car.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AppVersion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3402189502148599753L;

	private Long id;
	
	private String appId;

	private String lastVersion;

	private String nowVersion;

	private String versionName;

	private String description;

	private String platform;
	
	private String downloadUrl;

	private Byte updateType;

	private String updateTitle;

	private String creator;

	private Date createTime;

	private String updator;

	private Date updateTime;

	@Tolerate
	public AppVersion() {
	}
}
