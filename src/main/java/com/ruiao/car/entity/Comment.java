package com.ruiao.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 评论
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-16
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * appid
     */
    private String appId;

    /**
     * 1-文章，2-评论
     */
    private Integer hostType;

    /**
     * 文章的自增ID 或者 评论的自增ID
     */
    private String hostId;

    /**
     * 评论内容
     */
    private String commentContent;

    /**
     * 操作用户id
     */
    private Long accountId;

    /**
     * 操作用户名称
     */
    private String accountName;

    /**
     * 操作用户图像
     */
    private String accountAvatar;

    /**
     * ip地址
     */
    private String ipAddress;

    /**
     * 0删除，1成功
     */
    private Integer commentStatus;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 更新日期
     */
    private Date updateDate;

    @Tolerate
    public Comment() {
    }
}
