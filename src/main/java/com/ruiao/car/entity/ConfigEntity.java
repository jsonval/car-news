package com.ruiao.car.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruiao.car.enums.ConfigTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
@Data
public class ConfigEntity<T>{

    private String paramCode;
    private T paramValue;
    private String paramDesc;
    private String  osType;
    private String  minVersion;
    private String  maxVersion;
    @JsonIgnore
    private String  version;
    private Integer  configVersion;
    private Map<String,LinkedHashMap> dynamicParam;

    private String isValid;
    private String appId;
    private ConfigTypeEnum configType;

    private Long id;
    @JsonIgnore
    private Date createTime;
    @JsonIgnore
    private String createBy;
    @JsonIgnore
    private Date updateTime;
    @JsonIgnore
    private String updateBy;


    public ConfigEntity(String paramCode, T paramValue, String paramDesc, String osType, String appId, ConfigTypeEnum configType) {
        this.paramCode = paramCode;
        this.paramValue = paramValue;
        this.paramDesc = paramDesc;
        this.osType = osType;
        this.configVersion = 0;
        this.isValid = "1";
        this.appId = appId;
        this.configType = configType;
        this.createTime = new Date();
        this.createBy = "sys";
    }
}
