package com.ruiao.car.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户登录auth
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AccountAuth implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * appId
     */
    private String appId;

    /**
     * 对应用户id
     */
    private Long accountId;

    /**
     * 平台id
     */
    private String platformId;

    /**
     * 登录渠道
     */
    private String channel;

    /**
     * 平台 1 android 2 ios  3 h5 4 官网
     */
    private String os;

    private String ip;

    /**
     * 设备号
     */
    private String device;

    /**
     * token
     */
    private String auth;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 真假用户
     */
    private Boolean fake;

    /**
     * 1显示，0隐藏
     */
    private Boolean status;


}
