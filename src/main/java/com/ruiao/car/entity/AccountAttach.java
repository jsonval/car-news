package com.ruiao.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户附加属性表，存储积分等等信息
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
public class AccountAttach implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * appid
     */
    private String appId;

    /**
     * 用户ID
     */
    private Long accountId;

    /**
     * 总积分
     */
    private BigDecimal integralTotal;

    /**
     * 1正常
     */
    private Integer attachStatus;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 更新日期
     */
    private Date updateDate;

    /**
     * mlt地址
     */
    private String mltAddress;

    @Tolerate
    public AccountAttach() {
    }
}
