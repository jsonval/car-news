package com.ruiao.car.entity;
import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;
/**
 * <p>
 * 用户系统
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-04-28
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Account implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2101060687014836444L;

	/**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    
    private String appId;

    /**
     * 用户唯一id
     */
   // private String unionId;

    /**
     * 手机号
     */
    private String mobilePhone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    /**
     * 兑换密码
     */
    private String exchangePwd;

    /**
     * 头像
     */
    private String avatar;

    /**
     *
     */
    private String nickName;

    /**
     *
     */
    private String autoGraph;

    /**
     * 真实名称
     */
    private String realName;

    /**
     * 性别
     */
    private String gender;

    /**
     *
     */
    private String country;

    /**
     *
     */
    private String province;

    /**
     *
     */
    private String city;

    /**
     *
     */
    private String area;

    /**
     * 用户状态 show正常 del 删除 black拉黑 frozen 冻结
     */
    private String accountStatus;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 注册时间
     */
    private Date regTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 注册平台  android  ios   h5
     */
    private String osType;

    /**
     * 绑定平台
     */
    @TableField(fill = FieldFill.UPDATE)
    private String bindOsType;
    /**
     * 注册来源 main
     */
    private String appChannel;

    /**
     * 机器识别码
     */
    @TableField(fill = FieldFill.UPDATE)
    private String deviceId;

    /**
     * 真假用户
     */
    private Boolean fake;

    /**
     *
     */
    private String ipAddress;

    /**
     * 邀请码
     */
    private String invitationCode;

    /**
     * 用户等级
     */
    private String level;

    /**
     * 实名认证状态
     */
    private Boolean cerStatus;

    /**
     * 密钥
     */
    private String secretKey;


    private String wxAccount;

    private Boolean invitation;

    private String introduction;

    @Tolerate
    public Account() {
    }

}
