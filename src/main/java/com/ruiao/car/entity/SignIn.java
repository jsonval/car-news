package com.ruiao.car.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 签到
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-08-11
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SignIn implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * appid
     */
    private String appId;

    /**
     * 操作用户id
     */
    private Long accountId;

    /**
     * 签到日期
     */
    private String signInDate;

    /**
     * 0删除，1成功
     */
    private Integer status;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    @Tolerate
    public SignIn() {
    }

}
