package com.ruiao.car.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ruiao.car.utils.DateUtils;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 平台内容
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Articles implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    private String id;

    private String sourceId;

    /**
     * 封面数量
     */
    private Integer coverCount;

    /**
     * 封面图片数组
     */
    private String coverImages;

    private String title;

    private String abstractContent;

    private String author;

    /**
     * 发布用户ID
     */
    private String accountId;

    /**
     * 是否开启评论：0，不开启，1，开启
     */
    private Integer isOpenComment;

    /**
     * 是不是开启打赏：0,不开启，1开启
     */
    private Integer isOpenPayment;

    private Date publishTime;

    /**
     * 发布时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 是否删除：0，删除；1，正常
     */
    private Boolean status;

    /**
     * 发布源名称
     */
    private String origin;

    /**
     * 来源 1爬虫 2 用户自发 3 其他平台发布
     */
    private Integer sourceType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否真实的匹配上（0：假，1：真）
     */
    private String attr1;

    /**
     * 预留字段2
     */
    private String attr2;

    private Long sortValue;

    private String videoUrl;

    @TableField(exist = false)
    private ArticlesContent articlesContent;

    @TableField(exist = false)
    private int pv;

    /**
     * 1-已点赞
     */
    @TableField(exist = false)
    private int isPraise;

    /**
     * 1-已收藏
     */
    @TableField(exist = false)
    private int isCollect;

    /**
     * 1-已获取积分
     */
    @TableField(exist = false)
    private int isIntegral;

    @Tolerate
    public Articles() {

    }

    /**
     * 虚构阅读数量
     *
     * @param articlesOperate
     */
    public void rebuildPv(ArticlesOperate articlesOperate) {
        int realPv = articlesOperate == null ? 0 : articlesOperate.getPv();
        if (publishTime != null) {
            long diffMinute = DateUtils.diffMinute(publishTime, new Date());
            if (diffMinute >= 30) {
                Long addNum = Long.parseLong(id) % 2000 + 3000;//获取0-200的值
                this.pv = realPv + addNum.intValue();
            } else {
                Long addNum = Long.parseLong(id) % 90;
                Long vPv = diffMinute * addNum;
                this.pv = realPv + vPv.intValue();
            }
        }
    }

}
