package com.ruiao.car.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 平台内容频道
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ArticlesChannel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * appId
     */
    private String appId;

    /**
     * 内容自增id
     */
    private String articlesId;

    /**
     * 平台频道自增id
     */
    private String channelId;

    /**
     * 频道名称
     */
    private String channelName;

    /**
     * 状态  0：不可用  1：可用
     */
    private boolean status;

    /**
     * 创建时间
     */
    private Date createTime;


    private Date publishTime;

    /**
     * 备注
     */
    private String remark;


    @TableField(exist = false)
    private Articles articles;

    @Tolerate
    public ArticlesChannel() {

    }


}
