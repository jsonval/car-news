package com.ruiao.car.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户附加属性表 变更记录表
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
public class AccountAttachDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * appid
     */
    private String appId;

    /**
     * 用户ID
     */
    private Long accountId;

    /**
     * 记录类型 1-积分
     */
    private Integer attachType;

    /**
     * 记录值
     */
    private BigDecimal attachValue;

    /**
     * 来源ID
     */
    private String sourceId;

    /**
     * 1-文章
     */
    private Integer sourceType;

    /**
     * 1正常 0删除
     */
    private Integer status;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 更新日期
     */
    private Date updateDate;

    @Tolerate
    public AccountAttachDetail() {
    }
}
