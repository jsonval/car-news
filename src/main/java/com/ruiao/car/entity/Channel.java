package com.ruiao.car.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 平台频道
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Channel implements Serializable {

    @TableId(type = IdType.INPUT)
    private String id;

    private static final long serialVersionUID = 1L;

    /**
     * appId
     */
    private String appId;

    /**
     * 频道类型
     */
    private Integer channelType;

    /**
     * 频道名称
     */
    private String channelName;

    /**
     * 0:不推荐,1:推荐
     */
    private Integer channelAdvance;

    /**
     * 排序
     */
    private Integer channelSort;

    /**
     * 0，不可见，1可见
     */
    private Boolean channelStatus;

    private String userName;

    /**
     * 添加时间
     */
    private Date createTime;


}
