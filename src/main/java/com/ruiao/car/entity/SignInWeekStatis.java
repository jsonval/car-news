package com.ruiao.car.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 签到周期统计
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-08-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
public class SignInWeekStatis implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * appid
     */
    private String appId;

    /**
     * 操作用户id
     */
    private Long accountId;

    /**
     * 上一次签到日期
     */
    private String signInDateLast;

    /**
     * 连续签到累计
     */
    private Integer signCycleCount;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    @Tolerate
    public SignInWeekStatis() {
    }

}
