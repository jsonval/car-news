package com.ruiao.car.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ruiao.car.utils.SnowflakeIdWorker;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 内容基础数据
 * </p>
 *
 * @author jsonval@163.com
 * @since 2020-06-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ArticlesOperate implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    private Long id;

    /**
     * 内容自增id
     */
    private String articlesId;

    /**
     * pv,真实pv
     */
    private Integer pv;

    /**
     * bpv
     */
    private Integer uv;

    /**
     * praise
     */
    private Integer praise;

    /**
     * 评论数
     */
    private Integer comment;

    /**
     * 分享数量
     */
    private Integer share;

    /**
     * 踩
     */
    private Integer step;

    private Integer play;

    /**
     * 状态  0：不可用  1：可用
     */
    private Boolean status;

    /**
     * 创建时间
     */
    private Date createTime;

    public ArticlesOperate() {

    }


    public ArticlesOperate(String articlesId) {
        this.setId(SnowflakeIdWorker.getInstance().nextId());
        this.setArticlesId(articlesId);
        this.setPv(1);
        this.setUv(1);
        this.setPraise(0);
        this.setPlay(0);
        this.setComment(0);
        this.setShare(0);
        this.setStep(0);
        this.setStatus(true);
        this.setCreateTime(new Date());
    }


}
