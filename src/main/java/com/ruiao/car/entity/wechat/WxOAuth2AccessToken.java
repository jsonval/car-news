package com.ruiao.car.entity.wechat;

/**
 * Created by jsonval on 2019/12/7.
 */
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WxOAuth2AccessToken {
    @JsonProperty("errcode")
    private int errCode;
    @JsonProperty("errmsg")
    private String errMsg;
    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("expires_in")
    private int expiresIn;
    @JsonProperty("refresh_token")
    private String refreshToken;
    @JsonProperty("openid")
    private String openId;
    private String scope;
    @JsonProperty("unionid")
    private String unionId;

}
