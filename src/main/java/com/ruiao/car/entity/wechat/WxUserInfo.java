package com.ruiao.car.entity.wechat;

/**
 * Created by jsonval on 2019/12/7.
 */
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class WxUserInfo {
    @JsonProperty("errcode")
    private int errCode;
    @JsonProperty("errmsg")
    private String errMsg;
    @JsonProperty("openid")
    private String openId;
    @JsonProperty("nickname")
    private String nickName;
    private int sex;
    private String language;
    private String city;
    private String province;
    private String country;
    @JsonProperty("headimgurl")
    private String headImgUrl;
    private List<String> privilege;
    @JsonProperty("unionid")
    private String unionId;

}
