package com.ruiao.car.constant;

import java.math.BigDecimal;

/**
 * Created by jsonval on 2020/4/28.
 */
public class Constant {

    public static final String SUCCESS = "SUCCESS";
    public static final String success = "success";
    
    public static final String TOKEN = "carToken:";//token前缀


    public static final String TASK_CONFIG_CODE = "200000";

    public static final String WITHDRAW_COND = "400400";

    public static final String CARD_COND = "700000";


    public static final String default_avatar = "https://beenews-group.oss-cn-shanghai.aliyuncs.com/fad7f19d7cfaa1d1255260fc3ba278d5.png";


    public static final BigDecimal tkerReward = new BigDecimal(10);

    public static final String MLT_PRE = "MLT";

    public static final BigDecimal MLT_PRE_GIVE_RATE = new BigDecimal(0.9);

}
