package com.ruiao.car.enums;

/**
 * Created by jsonval on 2019/10/12.
 */
public enum AccountStatusEnum {
    show,
    del,
    frozen,
    black;
}
