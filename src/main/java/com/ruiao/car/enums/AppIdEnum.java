package com.ruiao.car.enums;

import org.apache.commons.lang.StringUtils;

/**
 * Created by jsonval on 2020/6/12.
 */
public enum AppIdEnum {
    other("",""),
    carNews("carNews","http://raimage.shiziqiu.com/pinchedi-logo.jpeg");


    private String appId;
    private String avatar;

    AppIdEnum(String appId, String avatar) {
        this.appId = appId;
        this.avatar = avatar;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public static AppIdEnum getAppIdEnum(String appId) {
        if(StringUtils.isNotBlank(appId)){
            for (AppIdEnum appIdEnum : AppIdEnum.values()) {
                if (appIdEnum.appId.equals(appId)) {
                    return appIdEnum;
                }
            }
        }
        return AppIdEnum.other;
    }

    public static void main(String[] args) {
        System.out.println(AppIdEnum.getAppIdEnum("carNews").getAvatar());
    }
}
