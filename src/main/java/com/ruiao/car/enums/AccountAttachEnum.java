package com.ruiao.car.enums;

import java.math.BigDecimal;

/**
 * @author yinpingliu
 * @name AccountAttachEnum
 * @time: 2020/6/28 下午3:26
 * @description: No Description
 */
public enum AccountAttachEnum {

    /**
     * attachType：1-积分类型
     * attachValue：1-积分值
     * sourceType：1-文章
     */
    ARTICLES_INTEGRAL(1,new BigDecimal("1"),1),
    /**
     * attachType：1-积分类型
     * attachValue：
     * sourceType：2-主动转赠
     */
    GIVE_INTEGRAL(1,null,2),
    /**
     * attachType：1-积分类型
     * attachValue：
     * sourceType：3-接受转赠
     */
    GIVED_INTEGRAL(1,null,3),
    /**
     * attachType：1-积分类型
     * attachValue：1-积分值
     * sourceType：3-激励视频
     */
    INCENTIVE_VIDEO_INTEGRAL(1,new BigDecimal("1"),4),

    /**
     * attachType：1-积分类型
     * attachValue：1-积分值
     * sourceType：5-签到奖励
     */
    SIGN_IN_DAY_1(1,new BigDecimal("1"),5),
    SIGN_IN_DAY_2(1,new BigDecimal("2"),5),
    SIGN_IN_DAY_3(1,new BigDecimal("3"),5),
    SIGN_IN_DAY_4(1,new BigDecimal("4"),5),
    SIGN_IN_DAY_5(1,new BigDecimal("5"),5),
    SIGN_IN_DAY_6(1,new BigDecimal("6"),5),
    SIGN_IN_DAY_7(1,new BigDecimal("7"),5),

    ;

    /**
     * 记录类型
     */
    private Integer attachType;

    /**
     * 记录值
     */
    private BigDecimal attachValue;

    /**
     * 来源类型
     */
    private Integer sourceType;

    AccountAttachEnum(Integer attachType, BigDecimal attachValue, Integer sourceType) {
        this.attachType = attachType;
        this.attachValue = attachValue;
        this.sourceType = sourceType;
    }


    public Integer getAttachType() {
        return attachType;
    }

    public void setAttachType(Integer attachType) {
        this.attachType = attachType;
    }

    public BigDecimal getAttachValue() {
        return attachValue;
    }

    public void setAttachValue(BigDecimal attachValue) {
        this.attachValue = attachValue;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }
}
