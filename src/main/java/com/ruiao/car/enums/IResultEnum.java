package com.ruiao.car.enums;

/**
 * Created by jsonval on 2019/10/15.
 */
public interface IResultEnum {
    public Integer getErrCode();

    public String getErrMsgEn();

    public String getErrMsg();

}
