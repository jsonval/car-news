package com.ruiao.car.enums;

public enum ResultEnum implements IResultEnum {


    /**
     * base
     */
    SUCCESS(Integer.valueOf(0), "成功", "success"),
    SYSTEM_ERROR(Integer.valueOf(9999), "网络异常", "network error"),
    ILLEGAL_PARAM(Integer.valueOf(9998), "非法参数", "illegal param"),
    LOSE_PARAM(Integer.valueOf(9997), "缺少参数", "lose param"),
    NO_DATA(Integer.valueOf(9996), "没有数据", "no data"),
    NO_LOGIN(Integer.valueOf(9995), "没有登录", "no login"),
    NO_AUTHORIZATION(Integer.valueOf(9994), "未授权", "no authorization"),
    ILLEGAL_REQUEST(Integer.valueOf(9993), "非法操作", "ILLEGAL REQUEST"),
    TOO_MANY_REQUEST(Integer.valueOf(9992), "请求过于频繁", "TOO_MANY_REQUEST"),
    ILLEGAL_OPERATE(Integer.valueOf(9991), "非法操作", "ILLEGAL OPERATE"),
    MEMBER_NOT_EXIT(Integer.valueOf(9990), "用户不存在", "User Non-existent"),
    REPEAT_SUBMISSION(Integer.valueOf(9989), "重复提交", "Repeat submission"),
    OPERATE_FAIL(Integer.valueOf(9988), "操作失败", "OPERATE FAIL "),
    OVER_LIMIT(Integer.valueOf(9987), "达到上限", "Over limit "),
    IMG_TOO_BIG(Integer.valueOf(9986), "图片太大", "IMG TOO BIG"),
    NO_PERMISSION(9985, "无权限", "No permission"),
    SIGN_ERROR(9986, "签名错误", "Sign error"),
    OUT_TRADE_NO_USED(9987, "商户订单号重复", "OUT_TRADE_NO_USED"),
    CODE_ERROR(9988, "验证码错误", "Code Error."),
    wx_bound(9827, "此微信已绑定其他账号，请更换其他微信号尝试", "wx bound ."),
    wx_error(9826, "微信登录异常", "wx error ."),

    NETWORK_ERROR(999, "系统错误", "Network Error."),

    already_invitation(9825, "已经填写邀请码", "already_invitation"),
    not_own_invitation_code(9824, "不允许填写自己的邀请码", "not_own_invitation_code"),
    invitation_code_error(9823, "请填写正确邀请码", "invitation_code_error"),


    task_config_error(9822, "任务配置错误", "task_config_error"),
    balance_not_enough(9821, "余额不足", "balance not enough"),
    operate_fail(9820, "操作失败", "operate fail"),

    SUBMIT_OVER_LIMIT(9819, "提交次数超过上限", "submit over limit"),
    AMOUNT_OVER_LIMIT(9818, "金额不在范围内", "amount over limit"),
    wx_unbound(9817, "未绑定微信", "wx_unbound"),


    CONFIG_ERROR(9816, "提现配置错误", "CONFIG_ERROR"),
    NOVICE_LIMIT(9815, "新手提现已达上限", "NOVICE_LIMIT"),
    WITHDRAW_LIMIT(9814, "今日提现已达上限", "WITHDRAW_LIMIT"),
    DAY_WITHDRAW_LIMIT(9813, "每天只能提现1次", "DAY_WITHDRAW_LIMIT"),
    username_pwd_error(9814, "用户名或密码错误", "Wrong user name and password"),
    Mobile_error(9814, "手机号码已存在", "mobile is extis"),

    secondsLimit(8000, "秒级限制", "secondsLimit"),
    dailyNumLimit(8001, "每日完成次数限制", "dailyNumLimit"),
    regTimeLimit(8002, "注册时间限制", "regTimeLimit"),
    relationTaskLimit(8003, "关联任务限制", "relationTaskLimit"),

    certified(7000, "用户已认证", "certified"),
    cerLimit(7001, "今日认证次数已达上限", "cer limit"),
    cerError(7002, "认证错误", "cer error"),
    cerRepeat(7003, "用户认证身份已在其他账号认证", "cer repeat"),
    config_error(7004, "配置错误", "config error"),
    activity_time_error(7005, "活动已失效", "activity time error"),
    activity_completed(7006, "活动已完成", "activity_completed"),
    lack_card(7007, "缺少卡片", "lack card"),
    acc_activity_completed(7008, "赠送用户已完成任务，无法接受卡片", "acc_activity_completed"),
	CODE_SMS_FAILED(6001,"短信发送失败", "code_sms_failed"),
	CODE_SMS_NOEXIST(6002,"验证码不存在", "code_sms_noexist"),
	CODE_SMS_OFTEN(600,"短信验证码发送过于频繁","code_sms_often"),
	CODE_AUTHCODE_FAILED(6003,"验证码不正确", "code_authcode_failed"),
	CODE_AUTHCODE_INVALID(6004,"验证码已失效", "code_authcode_invalid"),
	CODE_ACCOUNT_LOCK(2003,"账号异常已被锁定，请联系管理员","code_account_lock"),
	CODE_REG_FAILED(2006,"注册失败", "reg_error");


    private Integer errCode;
    private String errMsg;
    private String errMsgEn;

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrMsgEn() {
        return errMsgEn;
    }

    public void setErrMsgEn(String errMsgEn) {
        this.errMsgEn = errMsgEn;
    }

    ResultEnum(Integer errCode, String errMsg, String errMsgEn) {
        this.errCode = errCode;
        this.errMsg = errMsg;
        this.errMsgEn = errMsgEn;
    }

    @Override
    public String toString() {
        return "{errCode:" + this.errCode + ",errMsg:" + this.errMsg + "," +
                "errMsgEn:" + this.errMsgEn + "}";
    }


    public ResultEnum MsgRebuild(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }

}
