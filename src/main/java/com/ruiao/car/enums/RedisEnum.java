package com.ruiao.car.enums;

/**
 * Created by jsonval on 2019/7/1.
 */
public enum RedisEnum {
    CONFIG_CODE_VAL("配置code value"),

    ;
    private String desc;

    RedisEnum() {
    }

    RedisEnum(String desc) {
        this.desc = desc;
    }


    public static final String platform = "ruiao-car";


    public static final String groupSymbol = ":";

    /**
     * @param customKeys ->
     * @param customKeys
     * @return
     */
    public String getRedisKey(Object... customKeys) {
        String baseKey = platform + groupSymbol + this.name();
        for (Object customKey : customKeys) {
            baseKey = baseKey + groupSymbol + customKey.toString();
        }
        return baseKey;

    }


}
