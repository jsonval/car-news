package com.ruiao.car.enums;

import org.apache.commons.lang.StringUtils;

public enum OsTypeEnums {
    defaultVal(new Byte("0"),"默认"),
    android(new Byte("1"),"安卓"),
    ios(new Byte("2"),"IOS"),
    h5(new Byte("3"),"H5"),
    op(new Byte("4"),"管理后台"),
    server(new Byte("5"),"服务端"),
    anonymous(new Byte("6"),"匿名")
    ;
    private Byte code;
    private String msg;



    OsTypeEnums(Byte code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public static OsTypeEnums fromPlatform(String name) {

        if(StringUtils.isNotBlank(name)){
            for (OsTypeEnums osTypeEnums : OsTypeEnums.values()) {
                if (osTypeEnums.name().equals(name)) {
                    return osTypeEnums;
                }
            }
        }

        return OsTypeEnums.defaultVal;

    }

}
