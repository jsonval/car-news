package com.test.jpush;
import java.util.Map;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import cn.jpush.api.push.model.notification.Notification.Builder;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class JpushUtil {

	protected static final String APP_KEY = "391af603b2ff36610f9ac93a";
	protected static final String MASTER_SECRET = "e41f5068ee489389d43b6e9e";
	private static final String env = "test";
	
	public static PushResult pushMsgAll(String platFormStr, String message,String title,Map extra) {
		Platform  platForm = Platform.all();;
			
		Notification notif = buildNotFication(platFormStr, title, 1, null,extra);

		Options options = buildOptions(env);

		PushPayload pushLoad = buildPushObject(Audience.all(), platForm, notif, message, options);
		return JpushPushPayload(pushLoad);
	}
	
	
	public static PushResult pushMsgByUserNum(String platFormStr, String message,String title,String userNum,Map<String,String> extra) {
		Audience alis = buildAudience(userNum);
		Platform platForm = null;
		if (platFormStr.equalsIgnoreCase("IOS")) {
			platForm = Platform.ios();
		}
		if (platFormStr.equalsIgnoreCase("android")) {
			platForm = Platform.android();
		}
		if (platFormStr.equalsIgnoreCase("all")) {
			platForm = Platform.all();
		}
		
		Notification notif = buildNotFication(platFormStr, title, 1, null,extra);

		Options options = buildOptions(env);

		PushPayload pushLoad = buildPushObject(alis, platForm, notif, message, options);
		return JpushPushPayload(pushLoad);
	}
	
	
	public static Audience buildAudience(String tags) {
		return Audience.alias(tags);
	}
	
	public static PushPayload buildPushObject(Audience tag, Platform platForm, Notification notif, String msg,
			Options options) {
			return PushPayload.newBuilder().setPlatform(platForm).setAudience(tag).setNotification(notif)
					.setMessage(Message.content(msg)).setOptions(options).build();
		
	}
	
	 public static Notification buildNotFication(String platform, String ALERT, Integer badge, String sound, Map<String,String> extras) {
			Builder builder=Notification.newBuilder().
					addPlatformNotification(AndroidNotification.newBuilder().setAlert(ALERT).addExtras(extras).build());
			builder.addPlatformNotification(IosNotification.newBuilder().setAlert(ALERT)
					.setBadge(badge).setSound(sound).addExtras(extras).build());
			
			return builder.build();
	}
	 
	 public static Options buildOptions(String env) {
			if(env.equals("test")){
				return (Options.newBuilder().setApnsProduction(false).build());
			}
			return (Options.newBuilder().setApnsProduction(true).build());
		}
	 
	 public static PushResult JpushPushPayload(PushPayload pushLoad) {
			PushResult result = null;
			JPushClient jpushClient = new JPushClient(MASTER_SECRET, APP_KEY);
			try {
				log.info("Got call - " + pushLoad.toJSON());
				result = jpushClient.sendPush(pushLoad);
				
				log.info("Got result - " + result);

			} catch (APIConnectionException e) {
				log.error("Connection error, should retry later", e);

			} catch (APIRequestException e) {
				log.error("Should review the error, and fix the request", e);
				log.info("HTTP Status: " + e.getStatus());
				log.info("Error Code: " + e.getErrorCode());
				log.info("Error Message: " + e.getErrorMessage());
			}
			return result;
		}

}
