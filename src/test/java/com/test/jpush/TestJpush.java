package com.test.jpush;

import java.util.Map;

import org.junit.Test;

import com.google.common.collect.Maps;

public class TestJpush {
	
	/**
	 * 给所有用户推送消息
	 * @Title: pushMsgAll   
	 * @Description:        
	 * @return: void      
	 * @throws
	 */
	@Test 
	public void pushMsgAll() {
		JpushUtil.pushMsgAll("android","更多新品请登录山海菁APP查看。","山海菁上新品了",null);
	}
	
	@Test 
	public void pushMsgByUserNum() {
		Map<String,String> extra = Maps.newConcurrentMap();
		JpushUtil.pushMsgByUserNum("android","更多新品请登录山海菁APP查看。","山海菁上新品了","1",extra);
	}
	
}
